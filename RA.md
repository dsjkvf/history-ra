Roman Architecture by Diana E. E. Kleiner
=========================================


## Week 1
### Introduction: Maps, plans and the cultural impact
  * [Map of Rome](https://upload.wikimedia.org/wikipedia/commons/9/91/Roma_Plan_bw.jpg) [during Antiquity](https://upload.wikimedia.org/wikipedia/commons/c/c5/Roma_Plan.jpg) from G. Droysens Allgemeiner Historischer Handatlas, 1886
  * [Forma Urbis Romae](http://sights.seindal.dk/sight/290_Lanciani_Forma_Urbis_Romae.html)
  * [The Topography and Monuments of Ancient Rome](http://catholic-resources.org/AncientRome/Platner.htm), Boston: Allyn and Bacon, 1904
  * [Romanization](https://en.wikipedia.org/wiki/Romanization_(cultural))

### Lecture 2: The Founding of Rome and the Beginnings of Urbanism in Italy

  * Cities:
    * Two main streets, cardo, north-south, and decumanus, east-west
  * *Rome*: [The Temple of Jupiter Optimus Maximus Capitolinus](http://en.wikipedia.org/wiki/Temple_of_Jupiter_Optimus_Maximus)
    * 509 BC 
    * Rectangular structure, deep porch, free standing columns in that porch
    * The single staircase in the front
    * The tripart cella
    * Wood for the columns; mud/brick for the podium and for the walls; terracotta for the decoration
  * *Rome, alongside Tiberina*: [The Temple of Portunus](http://en.wikipedia.org/wiki/Temple_of_Portunus)
    * Late 2, early 1 century BC
    * Drew from both the Greek and the Etruscan religious architecture 
    * Etruscan: Tall podium, deep porch, free-standing columns in the porch, single staircase, facade orientation
    * Greek: Made out of stone, Ionic columns all around the structure (partly embedded in the wall, though), single cella
    * Roman, as a result: have columns and walls, which serve purpose (holding a flap or a roof), uses concrete (Opus Caementicium) in the podium, travertine for podium facing and columns, ashlar blocks of tufa for walls
  * *Cori*: Temple of Hercules
    * Once again, high podium, single starircase, facade orientation, deep porch, free-standing Doric columns (with [triglyphs](http://blogs.nd.edu/classicalarch/2012/07/15/the-mystery-of-the-triglyph/) and [metopes](https://en.wikipedia.org/wiki/Metope_(architecture))) in that porch (not fluted in the bottom: may be because of the fact people tended to to lean against these columns, or may be because of the decorative reasons: columns were painted at the bottom)
    * Pilasters in the wall instead of columns
    * Single cella
  * *Tivoli*: [Temple of Vesta](https://en.wikipedia.org/wiki/Temple_of_Vesta,_Tivoli)
    * Early 1 BC (80 BC?)
    * Round structure, rather high podium, single staircase (thus, a kind of facade orientation), central cella
    * Free-standing Corinthian columns, acanthus plant
    * Concrete (Opus Caementicium) for the podium, the central cella, Opus Incertum as the facing (travertine both for the facing and the columns)
    * Coffered ceiling

## Week 2
### Lecture 3: Technology and Revolution in Roman Architecture

  * *Rome*: [Porticus Aemilia](http://en.wikipedia.org/wiki/Porticus_Aemilia)
    * 193 BC
    * Made with concrete (Opus Caementicium), faced with Opus Incertum
    * Barrel vaults, opened walls, piers and arches above those piers to create arcades (enables both axial and lateral movement through the building)
    * Arched doors and arched windows, small curved windows on each tier
  * *[Ferentino](http://www.romeartlover.it/Ferentino.html)*: The Market Hall
    * 100 BC
    * One giant barrel vault; arched areas (also barrel vaulted), used as market stalls
    * Opus Incertum to form the vault and walls. Ashlar masonry (Opus Quadratum) on sides and the top of arch to emphasize he location and the shape, to reinforce strength and stability
    * A kind of prototype for Trajan Markets in Rome
  * *[Terracina](http://en.wikipedia.org/wiki/Terracina)*: [Sanctuary of Jupiter Anxur](http://www.italyheaven.co.uk/lazio/terracinatemple.html)
    * 100-70 BC
    * The podium: Opus Caementicium and Opus Incertum and Opus Quadratum for the facing, made up of a series of arcades, both axial and lateral access (smaller arcades in the side piers)
    * The cryptoporticus: an underground passageway, used for storage, etc: Opus Caementicium, a barrel vaulted corridor
    * The temple itself is a very traditional building
    * A colonnade behind (non-attached)
  * *Tivoli*: [Sanctuary of Hercules](http://www.tibursuperbum.it/eng/monumenti/tempioercole/index.htm)
    * 75-50 BC
    * The podium: Opus Caementicium and Opus Incertum and Opus Quadratum for the facing
    * The barrel vaulted cryptoporticus, used as a street underneath the sanctuary, with shops 
    * The circular, theatre-shaped staircase of the larger podium leads straight to the rectangular staircase of the temple itself; may been used as sits for some religious performances
    * The temple stands on its own, smaller, podium, reinforces an axial relationship to the larger podium
    * The temple's pushed up against the long back wall (is characteristic of forum design), which uses columns and arcades for the first story and then columns for the second story (Opus Caementicium)
  * *Rome (the north side of the Forum, the south slope of the Capitoline Hill)*: [Tabularium](http://en.wikipedia.org/wiki/Tabularium)
    * 78 BC
    * Used to store state archives
    * Opus Caementicium, Opus Quadratum with tufa stones for the facing
    * Arcades and columns, columns used only for the decoration
    * The insides utilize the barrel vaulted corridors; concrete for the domes, combined with the arches made out of stones supported by the columns
  * *Rome*: [The Theatre of Marcellus](http://www.mmdtkw.org/VTheatMarc.html)
    * 13-11 BC
    * The semicircular seating (cavea, divided into sections, cunei (cuneus))
    * Opus Caementicium, faced with travertine
    * Columns have no structural purposes whatsoever, serve only as decoration; the Doric order used for the first story, the Ionic order used for the second story (if there have been a third story, it should have utilize the Corinthian order)
    * Curved barrel (annular or ring) vaulted corridors resting on travertine piers; later the same vaults to be used in Colosseum
  * *Palestrina*: [Sanctuary of Fortuna Primigenia](http://www.romeartlover.it/Palestrina2.html)
    * 2-1 century BC
    * A combination of ramps and staircases (a natural hill cut back at levels to create terraces; barrel vaults were used, too)
    * Semicircular theatre at the top (hemicycle)
    * A very small round shrine at the very top (especially if compared to the whole structure)
    * Capitals sloped at the upper most part (had to confom the incline of the ramp)
    * Opus Caementicium, Opus Incertum
    * Alcoves for shops, hemicycles with columns made of tufa, travertine capitals
    * Annular (ring) vault
    * May be seen as a kind of pyramid with a shrine at the apex

### Lecture 4: Public buildings at Pompeii

  * Additional maps: [1](https://i.imgur.com/YIoGZXB.gif) & [2](https://i.imgur.com/5MMZ2oH.jpg)
  * [The Forum](http://www.flickriver.com/photos/16472880@N06/8000852136/#large)
  * The Capitolium
    * 150 BC the temple itself; renovated in 80 BC, the triple cella introduced
    * Made from tufa, usual Etruscan plan
  * The Basilica
    * 120 BC
    * The plan, the orientation and the ornamentation replicates the Forum itself
    * The first story uses Corinthian columns
    * And if there was a second story, it used the Corinthian order, too
    * While the first story of the wall columns were Ionic (and the supposed wall columns of the second story were again Corinthian)
    * Doesn't have a clerestory
  * The Amphitheater
    * The outer ring is made of concrete, faced with Opus Incertum
    * Encircled by an annular vault
    * The staircase with stairs leading from the both sides to the apex (no other one exists at other arenas)
    * Decorated by the series of arches or "blind arcades" (sometimes diminishing in size)
    * Two ways to enter, either via two barrel vaulted vomitoria, or by climbing via the staircase
  * The Theater and The Music Hall
    * 80-70 BC
    * The porticus: an open rectangular space with covered colonnades on either side for people to go during intermissions; had both shops and storage for theatrical stuff
    * The Music Hall had a roof because of acoustics
  * Bath complexes
    * Four key rooms: apodyterium (the dressing room), tepidarium (the warm room), caldarium (the hot room) and frigidarium (the cold room, round in shape with radiating alcoves; in a certain way, this may be seen as a foundation for the Pantheon later)

## Week 3
### Lecture 5: Houses and Villas at Pompeii

  * Additional maps: [1](https://sites.google.com/site/ad79eruption/pompeii/)
  * [House of the Surgeons](http://en.wikipedia.org/wiki/House_of_the_surgeon)
  * [House of Sallust](http://en.wikipedia.org/wiki/House_of_Sallust)
  * [House of the Vettii](http://en.wikipedia.org/wiki/House_of_the_Vettii)
    * The tablinum is almost gone
    * The garden is much more larger now
    * Columns in the Peristyle
  * [House of the Silver Wedding](http://en.wikipedia.org/wiki/House_of_the_Silver_Wedding)
    * Columns in the [atrium](http://en.wikipedia.org/wiki/Atrium_(architecture)) around the impluvium (tetra-style atrium)
    * Coulmns in the oecus (tetra-style oecus)
  * [House of the Faun](http://en.wikipedia.org/wiki/House_of_the_Faun): across the street from the House of the Vettii
    * The atrium with the First Style wall
    * Alexander exedra with Alexander Mosaic
  * [House of the Tragic Poet](http://en.wikipedia.org/wiki/House_of_the_Tragic_Poet)
    * Cave Canem Mosaic
  * [House of Menander](http://en.wikipedia.org/wiki/House_of_Menander)
  * [House of Pansa](https://sites.google.com/site/ad79eruption/pompeii/regio-vi/reg-vi-ins-6/house-of-pansa)
    * Many shops. Wasn't this one a prototype of a mall? With a pool specifically built to hold fish for sale
  * [House of Marcus Loreius Tiburtinus](http://en.wikipedia.org/wiki/House_of_Loreius_Tiburtinus)
    * Large garden
    * Second floor (story)
    * Grotto with the Third Style Wall Paintings
  * [Villa of the Mysteries](http://en.wikipedia.org/wiki/Villa_of_the_Mysteries): 400 metres northwest; also see below
    * The podium and the cryptocorticus udnerneath the podium (to muffle the sounds from the street, probably)
    * Curved panoramic windows

### Lecture 6: Second Style Roman Wall Painting (picture windows)

  * *Near Rome*: [Villa of Livia](https://en.wikipedia.org/wiki/Villa_of_Livia)
    * Garden Room: quintessential Second Style wall
  * *Southern Italy, [Boscoreale](https://sites.google.com/site/ad79eruption/boscoreale)*: [Villa of Publius Fannius Synistor](https://en.wikipedia.org/wiki/Villa_Boscoreale)
  * *Rome, Palatine Hill*: [House of Augustus](http://en.wikipedia.org/wiki/House_of_Augustus)
    * Room of the Masks
        * Second style: One point linear perspective introduced

## Week 4
### Lecture 7: Third and Fourth Style Roman Wall Painting

  * *Southern Italy, Torre Annunziata*: [Villa Poppaea](https://en.wikipedia.org/wiki/Villa_Poppaea) (or [Villa Oplontis](http://www.skenographia.cch.kcl.ac.uk/oplontis/))
    * Third style: no perspective, flat walls, frames, decorations (not a picture window, but a framed picture)
  * *Southern Italy, Boscotrecase*:  Villa of Agrippa Postumus
    * Red Room
    * Black Room
  * *Pompeii*: [House of the Orchard](https://sites.google.com/site/ad79eruption/pompeii/regio-i/reg-i-ins-9/house-of-the-orchard)
    * Garden Room
  * *Rome, under the ruins of the Baths of Trajan*: [Domus Aurea](http://en.wikipedia.org/wiki/Domus_Aurea)
    * Octagonal Room
    * Room of the Birds
    * Room 78
        * Arcitechture is re-introduced as a part of a decoration; Fourth Style emerges; "architectural cages"
  * *Pompeii*: [House of the Vettii](http://en.wikipedia.org/wiki/House_of_the_Vettii)
    * Garden Room Q
    * Pentheus Room
        * Middle Fourth Style
    * Ixion Room
        * Full glory Fourth Style
            * **First Style**: the socle, which is, attempts to imitate marble incrustation in paint.
            * **Second Style**:  the substantial columns that are located in the second tier or in the main tier of the painted wall. Columns that support a lintel above and a coffered ceiling.
            * **Third Style**: the mythological landscape in the center that has a black frame around it to make it abundantly clear that this is not a window to something else but rather meant to look as if it is a flat panel painting hanging on the wall (and also the floating mythological figure in the center in this case  of a white panel with a border that is made up of floral or vegetal motifs.
            * **Fourth Style**: the  introduction of architecture once again, on either side of the main panel in the main zone; not representations of complete buildings, but rather fragments of buildings depicted in illogical space; also the architectural cages  in the uppermost tier

### Lecture 8: Special Topics In Roman Wallpainting

  * *Pompeii, 400 metres northwest*: [Villa of The Mysteries](http://www.art-and-archaeology.com/timelines/rome/empire/vm/villaofthemysteries.html)
    * Room 16: Second Style
    * Room 5 or The Initiation Chamber: Fourth Style
  * *Pompeii*: [Villa of Julia Felix](https://en.wikipedia.org/wiki/House_of_Julia_Felix):
    * Second Style paintings, still pieces

## Week 5
### Lecture 9: Augustus[^1] Assembles Rome

From brick to marble (marble was discovered in Italy, see [Carrara](http://en.wikipedia.org/wiki/Carrara_marble) (or [Luna](http://en.wikipedia.org/wiki/Luna_(Etruria))) quarries)

  * *Rome*: [Forum](http://en.wikipedia.org/wiki/Forum_of_Caesar) [of Julius Caesar](http://en.wikiarquitectura.com/index.php/Forum_of_Julius_Caesar)
    * 46 BC (restored after fire by Domitian in the 1st century AD and then by Trajan in the early 2nd century AD (deep drill decorations, etc.)
    * Based heavily on the Forum of Pompeii and on the Caesareum in Alexandria (didn't survive) done by Cleopatra to praise Julius Caesar
    * Colonnade both on the right and on the left; additionally shops (tabernae) on the right; the Temple of Venus Genetrix at the top (on one of the short sides): has a high podium, a single staircase (though partially encircling the podium), facade oriented, has a deep porch, free-standing columns in that porch, single entrance, flat back wall
    * A statute of Cleopatra as an Egyptian goddess Isis in the Temple (right next to Venus)
    * Has fallen into decay later but restored again both by Domitian and then by Trajan sometime between 98 -- 117 AD
  * *Rome*: [Forum of Augustus](http://en.wikipedia.org/wiki/Forum_of_Augustus) (on the other side of Via dei Fori Imperiali - on the left if going from the Victor Emmanuel II Monument to the Colloseum)
    * 2 BC
    * Made of ashlar blocks of peperino tufa with Carrara marble
    * A rectangular space, open to the sky, colonnades on either side, the Temple of Mars Ultor in the center, pushed against the back wall and dominating the space in front of it
    * The only change from the traditional forum is the addition of the hemicycles on either side (see the sanctuary of Fortuna Primigenia at Palestrina); like embracing arms that serve to accentuate the temple
    * *[Temple of Mars Ultor](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/imperialfora/augustus/mars.html)*
        * A high podium (made out of tufa), a facade orientation, a single staircase, a deep porch, free-standing columns in that porch and on the either side, a flat back wall, some columns inside and a single (not tripart) niche; made from marble
        * A brick (peperino ashlar blocks) precinct wall to protect from fires from [Subura](http://en.wikipedia.org/wiki/Suburra)
    * Usually Corinthian columns, sometimes zoomorphic, sometimes conventionalized maidens inestead of formal columns
  * *Rome*: [Ara Pacis](https://en.wikipedia.org/wiki/Mausoleum_of_Augustus) (near the [Mausoleum of Augustus](https://en.wikipedia.org/wiki/Mausoleum_of_Augustus))
    * 9 BC
    * The monument is covered with all kinds of sculptural decoration, including flowering acantus, plants, mythological and historical scenes
    * Two entrances (on the eastern and on the western sides) but the only staircase on the western side (because of Athens legacy or because of Temple of Janus in the Roman Forum?)
    * The precinct wall is divided into zones: while the bottom mimics the wooden fence, the top has garlanded swags hanging from the pilasters and from the sculls of bulls (that represent bulls sacrificed at this altar) and the libation dishes above these swags (this ornament should have probably replicated the older wooden altar that was built there previously)
    * The outside has a series of square panels, four of them on the entrances' sides and also on the North and South sides (the frieze on the South side includes a portrait of Augustus himself)

### Lecture 10: Tombs of Roman Aristocrats, Freedmen, and Slaves

  * *Rome*: [Mausoleum of Augustus](https://en.wikipedia.org/wiki/Mausoleum_of_Augustus)
    * 28 BC, long before his real death; Augustus wasn't sure about his health and about how long is he going to live. Besides, the Mausoleus was planned as a family tomb
    * The central burial chamber, the hollow drum, the concentric rings around it, the outer wall; all made out of concrete, the wall faced with travertine (not marble)
    * Used to have annular vaults, and a tumulus, an Earth mound on top of those, with a bronze statue of Augustus at the very apex and trees planted on top of the mound
    * Also, see the [necropolis at Cerveteri](https://en.wikipedia.org/wiki/Cerveteri#Necropolis_of_the_Banditaccia)
    * Was used as a garden in the Middle Ages, also as a fortress by Colonna family, also as a bull ring and also as a music hall
  * *Rome*: [Tomb](http://www.worldsiteguides.com/europe/italy/rome/tomb-of-caecilia-metella/) [of Caecilia Metella](http://en.wikipedia.org/wiki/Tomb_of_Caecilia_Metella) at Via Appia
    * 20 BC
    * Basically, a cylindrical drum placed at some height via the concrete podium
    * Faced with travertine, frieze at the top, made out pentelic marble
    * Was used as a fortress and as a palace in the Middle Ages
  * *Rome*: [Tomb of Gaius Cestius](https://en.wikipedia.org/wiki/Pyramid_of_Cestius)
    * 15 BC
    * The only Roman tomb in the form of Egyptian pyramid survived
    * First placed outside Rome but as the city grew, in the 3rd century AD they've built the new wall, the famous Aurelian walls (alongside the tomb one can see both the remains of this wall and the gateway); nowadays at the back of the tomb the [Protestant Cemetery](https://en.wikipedia.org/wiki/Protestant_Cemetery,_Rome) is located with the likes of Percy Bysshe Shelley and John Keats buried there
    * The inner core of the tomb was made out of concrete, and the outer parameter shape was faced with travertine
    * The very small burial chamber inside (still, this was also a family tomb)
    * Third style wall paintings inside
    * Marcus Agrippa's also buried there
  * *Rome*: [Tomb](https://depts.washington.edu/hrome/Authors/peeter/TheEvolutionofaMonumentalSpaceTheBakersTombandPortaMaggiore/pub_zbarticle_view_printable.html) [of Eurysaces](http://en.wikipedia.org/wiki/Tomb_of_Eurysaces_the_Baker)
    * 50-20 BC
    * The travertine gate behind the grave was built later, during the time of the emperor Claudius
    * A three-storied structure: the first one is made out of tufa blocks, the second and the third stories are faced with travertine, and the interior is concrete
    * The second story features some cylinders placed vertically, and the third story faces cylinders again, yet faced horizontally this time (not columns, no capitals, very fat); represented grain measures, silos?
    * Trapezoidal in shape; placed between two major roads at that time, Via Labicana and Via Praenstina, which converged exactly at the facade of this tomb
    * The friezes tell us Euryscales himself was a baker and a contractor, sold bread to armies, made a fortune because of that
    * The marble relief portraying Euryscales himself and hid wife Atistia (similar to to way Augustus and Livia are sculptured in Ara Pacis)
    * The friezes in the great detail depicting the process of making the bread

## Week 6
### Lecture 11: Tiberius, Caligula, Claudius and Nero and Their Amazing Architectural Legacy

  * *[Baiae](https://en.wikipedia.org/wiki/Baiae)*: [Temple of Mercury](https://timelessitaly.wordpress.com/2013/10/22/ancient-ruins-of-baia/)
    * Dome made out of concrete
    * Light streams through the oculus in the dome
  * *Capri*: [Villa Jovis](http://en.wikipedia.org/wiki/Villa_Jovis)
    * 14-37 AD
    * Concrete
    * Has a series of barrel vaults in tiers, containing cisterns of the villa (the water supply for baths, kitchen, etc)
    * A pavement was placed on top of these barrel vaults to create a very large court
    * A hemicyled aula with large picture windows
    * A logia on the northern part, alongside the emperor's apartments
    * Also, an ambulatio, a long walk on the northern side (connected with the main building via the corridor) with a triclinium (or the dining room) in the center
  * *Rome*: [Underground Basilica](http://archeoroma.beniculturali.it/en/archaeological-site/underground-basilica-near-porta-maggiore)
    * The first decades of the 1st century AD (50 AD?)
    * Near the Tomb of Eurysaces, the_Baker
    * A central nave with two side aisles divided by that nave
    * [Piers](https://en.wikipedia.org/wiki/Pier_(architecture)), not columns
    * An [apse](https://en.wikipedia.org/wiki/Apse) at the end (to give some logical emphasis to this short side)
    * In order to create the Basilica, first, the trenches in the tufa rock were cut, and then the concrete was poured into those trenches to create the walls and the barrel vaults. And once the concrete has dried, it was cut in such a way as to create the piers
    * The walls are stuccoed over and divided into a series of Third Style Wall decorated panels (as is the ceiling)
  * *Rome*: [Portus](https://en.wikipedia.org/wiki/Portus)
    * 41-54 AD
    * Curving breakwaters made up of columns
        * Capitals resemble the Doric order
        * Bases are finished with moldings
        * A series of column drums piled one on top of another in between; these drums seem to be deliberately not finished, left in a rough, a rusticated state (also see Porta Maggiore for the same "finishing" of columns)
    * A colossal statue of Neptune in the center
    * A lighthouse alongside the statue
  * *Rome*: [Porta Maggiore](https://en.wikipedia.org/wiki/Porta_Maggiore)
    * 41-54 AD
    * Travertine, cut stone construction
    * Near the Tomb of Eurysaces, the Baker
    * Served as a crossing point for two aqueducts
    * Has a three-tiered attic with an inscription (describing Claudius, who has built it, and the aqueducts in question)
    * Arcuations surrounded by rust columns on either side supporting pediments above (pediments, lentils and capitols are finished, while the blocks of the columns and the blocks of the rest of the structure are left in a rusticated state); finished vs. unfinished masonry (like, a finished column is seeking to get out of this unfinished, rusticated masonry)
    * Corinthian capitals?
    * One can even see this rusticated architecture of Claudius as something that is extremely reflective of the peculiar personality of this man himself
    * Also, the whole cut stone construction was already very old-fashioned at the times of Claudius, thus, again, this may be seen as another trait of his character, looking back to the history of Rome and to the history of Etruscans
  * *Rome*: [Domus](http://en.wikipedia.org/wiki/Domus_Transitoria) [Transitoria](http://www.forumancientcoins.com/numiswiki/view.asp?key=Neros%20First%20Residence-%20DOMUS%20TRANSITORIA)
    * Sometime before the fire of 64 AD
    * Between the Palatine Hill and the Esquiline Hill (thus the naming, transitoria, the transition)
    * Mostly destroyed both by the fire and by the latest emperors; underground today
    * The Fountain Court
        * A pool with columns around it at the southern part
        * A fountain itself in a niche by the northern wall
        * This particular wall resembles the theatre architecture
        * Barrel vaulted rooms on both the eastern and western sides (special dining areas)
        * Pavements made out of marble, while walls were riveted with marble brought from all the parts of the world
    * The Domed Room
        * Again, the whole structure is based on [Frigidarium](http://en.wikipedia.org/wiki/Frigidarium) of Pompeii (concrete, round structure, has a dome and a oculus)
        * The pavement may be treated as a circle inscribed into an octagon
        * Expands via corridors in four sides, has columns in two of those corridors with metal grilles on top
  * *Rome*: [Domus](http://en.wikipedia.org/wiki/Domus_Aurea) [Aurea](http://www.greatbuildings.com/buildings/Domus_Aurea.html)
    * 64-68 AD
    * Brick face construction (concrete faced with brick)
    * Still buried under the Baths of Trajan and the surrounding park
    * The Octagonal Room
        * A series of different shape alcoves
        * Represents a break from the tyranny of the rectangle
        * Creates vistas in every direction
        * Creates one continuous envelope around interior space
        * Fully realizes the technical and expressive potentiality of Roman cement
        * Switches the emphasis from _solids_ to _voids_, from walls and roofs to the insubstantial space they enclose and shape
        * Natural light plays a key role
        * Originally decorated with marble from around the world and the Third and the Forth Style Wall paintings

### Lecture 12: The Colosseum and Contemporary[^2] Architecture in Rome

  * *Rome*: [Temple](http://archive1.village.virginia.edu/spw4s/RomanForum/GoogleEarth/AK_GE/AK_HTML/TS-040.html) [of Divine Claudius](http://www.maquettes-historiques.net/P38.html)
    * 70 AD
    * Two-storied platform with barrel vaulted chambers, made out of concrete (see the Sanctuary of Jupiter Anxur at Terracina), faced with cut stone travertine (see Portus, etc); a combination of smooth and rusticated masonry for the arches and pilasters in front of those arches (note that in between of each of these rusticated pilaster blocks, part of the smoothed pilaster emerges; think of a cocoon and a butterfly)
    * Doors on the first and windows on the second tier
    * Large rectangular space with a temple above the podium, surrounded by a lot of bushes (which becomes a very popular way of decorating in the Flavian period)
  * *Rome*: [Colosseum](http://en.wikipedia.org/wiki/Colosseum), or [Rome Flavian Amphitheatre](http://www.history.com/topics/ancient-history/colosseum)
    * 80 AD
    * Build on the former artificial lake made up by Nero
    * Named Colosseum because of the [Colossus](http://en.wikipedia.org/wiki/Colossus_of_Nero), the statue by Zenador that stood nearby
    * Could hold up to 50'000 people
    * Oval plan
    * [Made](http://docgeorge.net/world105/Docs/Rome_files/image005.jpg) out of concrete
    * Four tiers (Doric, Ionic and Corinthian columns for the first three stories, Corinthian pilasters for the forth one; no structural purpose whatsoever)
    * First story: a series of barrel and annular vaults (corridors, etc), which rest on travertine piers
    * Second story: a series of [groin (ribbed) vaults](https://www.oneonta.edu/faculty/farberas/arth/arth109/arch_vaults.html) (the innovation of the Flavian period)
    * The second and the third stories had statues placed in the niches beneath the arches
    * Was highly decorated (stucco and paintings; is stripped of the decorations today due to various reasons: was used as a marble quarry, etc, etc)
    * Substructures below the arena, all made of concrete, were used for the storage of crops and for the housing of the animals (via a system of ramps and pulleys)
    * The arena itself was covered with some ancient version of astroturf, had trees, bushes and hills there
    * All the seats were sheathed with marble in antiquity
  * *[Pozzuoli](http://en.wikipedia.org/wiki/Pozzuoli)*: [Amphitheater](http://en.wikipedia.org/wiki/Flavian_Amphitheater_(Pozzuoli))
    * Late 1 century AD, very well preserved
    * Substructures of the amphitheater: concrete faced with brick, the annual vaulted corridors, cages for the animals
    * The ceiling of these substructures has openings through which the animals were transported up to the arena
  * *Rome*: [Imperial Fora](https://en.wikipedia.org/wiki/Imperial_fora)
    * [Forum](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/imperialfora/vespasian/templumpacis.html) [Pacis](http://romereborn.frischerconsulting.com/ge/FR-008.html)
        * Hasn't survived
        * Rectangular form, a temple placed on one of the relatively long sides, on the south-western wall, rectangular alcoves by the sides, columns all the way around
        * Red granite columns for the colonnade
        * Yellow columns for the columns that screen alcoves
        * White marble for the rest
        * Bushes in front of the temple, a kind of garden
        * May have served as a museum (trophies from the conquest of Jerusalem by Titus may have been displayed there)
  * *Rome*: [Arch](https://en.wikipedia.org/wiki/Arch_of_Titus) [of Titus](http://www.bluffton.edu/~sullivanm/titus/titus.html)
    * Built from the Pentelic marble but then travertine was used for the restoration by Valadier
    * Was used as a fortress at some point in the Middle Ages
    * Letters were bronze, attached to the marble
    * The composite capital: a column capital that combine the Corinthian (acanthus leaves, flowers) and the Ionic orders (prominent volutes)
    * Two figural panels that display the scenes from the Titus' conquest of Jerusalem
    * Inside the attic of the arch is a staircase and a chamber that might have served as a burial chamber for Titus (and thus the arch itself might have been his tomb - though, the urn has never been found)
  * *Rome*: [Temple of Divine Vespasian and Titus](http://www.sacred-destinations.com/italy/rome-temple-of-vespasian)
    * 79-81 AD
    * Built by Titus in honor of Vespasian at his death, later rededicated by Domitian both to Vespasian and Titus
    * Only three Corinthian fluted columns of the temple still survive nowadays (right in front of the Tabularium)
    * However, was very richly decorated, deep drill was used, etc; resembles the Forth Style Wall Painting
  * *Rome*: [Baths](http://www.maquettes-historiques.net/P23.html) [of Titus](http://en.wikipedia.org/wiki/Baths_of_Titus)
    * 80 AD
    * Hasn't survived (all we have is one concrete wall, brick-faced, with some engaged columns)
    * West of the Esquiline Wing of the Nero's Golden House (later the Trajan Baths were built nearside)
    * The first example of so called imperial bath structure
    * A very elaborate entrance way, consisted either of columns on square basis, or piers in the front; also a series of groin vaults, leading in a double palestra
    * A lot of attention dedicated to symmetry: two rooms for bathing are at the center, other rooms symmetrically placed on either sides, frigidarium, being earlier the smallest room, now becomes the largest room (with the groin vault right over the center), right beside the bathing rooms, flanked and buttressed by two barrel vaults, one on the either side, with columns that screen those alcoves from the central groin vaulted space (see the Octagonal room, compare it with the frigidarium here)

## Week 7
### Lecture 13: Human Made Divine on the Palatine Hill

  * *Rome*: [Stadium](http://en.wikipedia.org/wiki/Stadium_of_Domitian) [of](http://www.romeanditaly.com/underground-rome-inside-domitian-stadium/) [Domitian](https://wilsonancientrome.wikispaces.com/Stadium+of+Domitian) // *(visit Tre Scalini nearby for the Italian ice cream, gelato: try tartufi)*
    * 92-96 AD
    * Nowadays buried under Piazza Navona
    * Long, hairpin shape (an elongated shape with one straight end and one curved end)
    * Only two tiers of arcades and columns
    * Brick faced cement construction
    * Travertine Ashlar masonry for arcades and columns
  * *Rome*: [Palace](http://rometour.org/palace-flavian-flavia-augustana-augustus-stadium-domitian-domiziano-palatine-hill.html) [of Domitian](https://en.wikipedia.org/wiki/Flavian_Palace)
    * 92 AD
    * Architected by [Rabirius](https://en.wikipedia.org/wiki/Rabirius_(architect))
    * Brick-faced concrete construction
    * Had a [hypocaust](http://en.wikipedia.org/wiki/Hypocaust)
    * Two wings, [the public one and the private one](http://www.timerime.com/content/timeline/807/~/~/1021785/)
    * The public wing, or the Domus Flavia, was of one story; featured an octagonal fountain in the peristyle (see Nero's Golden House)
    * Also, there were apses (half circles) in the basilica, the audience hall (aula) and the dining room (triclinium), all of which faced in one direction: a place for Domitian himself to sit, a place for Dominus Deus, for the lord and the god (his exact title)
    * The private wing, or the Domus Augustana, was of two stories
    * Includes some fantastically-shaped rooms; some of them cruciform, cross-shaped, circular rooms with radiating alcoves (again, see Nero's Golden House)
  * *Rome*: [Forum](http://en.wikipedia.org/wiki/Forum_of_Nerva) [Transitorium](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/imperialfora/nerva/transitorium.html)
    * 98 AD
    * Begun under Domitian as the Forum Transitorium (a point of transit between the Subura and the Roman Forum), then finished under Nerva[^3] as the Forum of Nerva
    * Featured the Temple of Minerva, the Domitian's patron goddess: frontal orientation, single staircase facade, free standing columns in the porch, etc (doesn't exist today since the material it was built from was taken and reused by Pope Paul the Fifth)
    * Previously a street, and therefore quite limited in space. Thus, since there was no space for a covered colonnade there, they've built columns (Corinthian capitols, projecting [entablature](http://en.wikipedia.org/wiki/Entablature), very highly decorated) very close to the wall (but not attached) and hadn't put any ceiling on top of those columns

### Lecture 14: Civic Architecture in Rome under Trajan

  * *Rome*: [Baths](http://en.wikipedia.org/wiki/Baths_of_Trajan) [of Trajan](http://www.roman-empire.net/tours/rome/baths-trajan.html)
    * 109 AD
    * Architected by Apollodorus of Damascus
    * Located on the Esquiline Hill and the Oppian Hill on top of the Nero's Domus Aurea
    * To a great extent the plan resembles the Baths of Titus (though extraordinary different in scale)
    * The bathing block is placed in a very large rectangular [precinct](http://en.wikipedia.org/wiki/Precinct)
    * This large rectangular precinct has a series of rooms around it, screened by columns; were used as tabernae, as meeting halls, lecture halls, Greek and Latin libraries
    * Also a large hemicycle in front of the building, with seats for spectators to watch the performances, etc (really, a theatre in a bath!)
  * *Rome*: [Forum](http://en.wikipedia.org/wiki/Trajan%27s_Forum) [of](http://www.ust.ucla.edu/ustweb/Projects/trajans_forum.htm) [Trajan](http://www.aviewoncities.com/rome/forumoftrajan.htm)
    * 106 AD
    * Architected by Apollodorus of Damascus
    * Had to eliminate the sides of the Quirinal and Capitoline (Campidoglio) Hills (which were 125 feet in height, exactly the height of the Column of Trajan)
    * Convexed facade
    * Corinthian columns
    * Figures of captured Dacians
    * The Basilica Ulpia lies at the north end of the piazza, which was cobbled with rectangular blocks of white marble and decorated by a large equestrian statue of Trajan. On either side of the piazza are markets, housed by the [exedrae](https://en.wikipedia.org/wiki/Exedra)
    * North of the Basilica was a smaller piazza, with a temple, built by Hadrian, dedicated to the deified Trajan on the far north side facing inwards. Directly north of the Basilica Ulpia on either side of the forum were two libraries, one housing Latin documents and the other Greek documents. Between the libraries was the 38 m Trajan's Column
    * *Rome*: [Basilica](http://en.wikipedia.org/wiki/Basilica_Ulpia) [Ulpia](http://www.proun-game.com/ForumTrajani/EN%20Basilica.html)
        * Central nave, divided by two side aisles
        * Corinthian colonnade
        * Flat roof with a corporate ceiling
        * Clear second story (see House of the Mozaic); opens up of the wall through Ionic columns
    * *Rome*: [Column](http://en.wikipedia.org/wiki/Trajan%27s_Column) [of Trajan](http://www.trajans-column.org/)
        * 112 AD
        * Flagged by the Latin and Greek libraries
        * The spiral staircase that leads from bottom to top
        * The staircase also goes down below into a burial chamber; two urns were found there, the urns of Trajan and his wife Plotina, which tells that this also served as Trajan's tomb
  * *Rome*: [Markets](http://en.wikipedia.org/wiki/Trajan%27s_Market) [of Trajan](http://www.greatbuildings.com/buildings/Markets_of_Trajan.html)
    * 113 AD
    * Concrete faced with brick
    * More than 150 shops on a variety of levels
    * Large windows opening up the space
    * A covered bazaar
    * A second story up here with tabernae opened almost completely to the sky
    * Now, they don't even need walls to support vaults, but can can lift their vault on top of individual piers (like opening up a series of umbrellas over the space)
  * *Benevento*: [Arch of Trajan](http://en.wikipedia.org/wiki/Arch_of_Trajan_(Benevento))
    * 114-118 AD
    * Clearly based on the Arch of Titus in Rome (much more sculpture, though)

## Week 8
### Lecture 15: Hadrian's Pantheon and Tivoli Retreat

  * *Rome*: [Temple of Venus and](http://en.wikipedia.org/wiki/Temple_of_Venus_and_Roma) [Roma](http://www.italian-architecture.info/ROME/RO-028.htm)
    * 135 AD, burned down during the fire in 283 AD, restored later by Maxentius
    * Probably architected by Hadrian himelf
    * Located on the Velian Hill (on Via Sacra, next to the Arch of Titus), between the eastern edge of the Forum Romanum and the Colosseum (so called Flavian area -- i.e., continues the tradition of returning to Roman people the land that was originally theirs but stolen by Nero)
    * Extremely different to a usual Roman temple (see all examples above)
    * Low podium
    * No facade orientation (got two facades)
    * Back-to-back two cellas, for Venus and Roma (because of two facades), either separated by a wall or by having two niches side-by-side
    * Columns and a staircase surround the whole structure (think Greek influence)
    * Large precinct that also has columns around it
    * Concrete faced with brick
    * Possibly barrel vaulted
    * Walls made scalloped possibly later, in the 4 century AD
    * Returns to use of Greek proconnesian marble (opposite to Augustus, who used marble from Luna or Carrara, and also to Nero and the Flavians, who used marble from all over the world)
  * *Rome*: [Pantheon](http://en.wikipedia.org/wiki/Pantheon,_Rome), Temple to all the Gods // *(Fortunato al Pantheon, try veal scallopini al gorgonzola; [Della Palma](http://www.dellapalma.it/en/), gelato: try zabaglione; also try [Giolitti](http://www.giolitti.it/en/our-stores) at Via Uffici del Vicario, 40, near the Montecitori Palace, or the Chamber of Deputies of the Italian Parliament (another one at Viale Oceania, 90, 00144 Roma, on the bank); also [San Crispinno](http://www.huffingtonpost.com/2010/08/16/san-crispino-gelato-eliza_n_683801.html) either near Trevi Fountain, Via della Panetteria, 42, 00187 Roma, or near the Pantheon again)*
    * 118-128 AD
    * Built on the top of the previous Pantheon (had a [caryatid](http://en.wikipedia.org/wiki/Caryatid) porch), commissioned by Marcus Agrippa (son-in-law of Augustus)
    * Concrete faced with brick (the severity of the structure relieved by adding three cornices and brick arches (arches that kept concrete from settling until it dries))
    * The recipe of concrete has changed for the first time since Caligula, adding basalt at the base of the dome, and then porous pumice at the top, allowing to decrease the thickness of walls (since they were pretty solid and the dome was made lighter)
    * There was a rectangular forecourt that had covered colonnades on either side
    * Possibly and Arch in the middle of this forecourt, and an entranceway on the side that was opposite to the Pantheon
    * Possibly an altar in front of the temple
    * Traditional Roman porch with the free-standing light gray granite columns (white Corinthian capitals) in that porch that support the [pediment](http://en.wikipedia.org/wiki/Pediment) (which in its own turn would screen the dome of the temple from the visitors)
    * High podium, single staircase, definitely facade orientation
    * Then, shielded by the porch, extremely innovative cylindrical drum, supported by a hemispherical dome
    * The circular drum was internally half the height of the diameter; was surmounted by a hemispherical dome, the crown of which is the same exact distance (the diameter of the Pantheon is 142 feet, while St. Peter's, for example, is only 139 feet in diameter)
    * Bronze doors
    * Dome with 5 rows of 28 coffers each
    * Light as decor
  * *Tivoli*: [Hadrian’s](http://en.wikipedia.org/wiki/Hadrian%27s_Villa) [Villa](http://www.greatbuildings.com/buildings/Hadrians_Villa.html) [at](https://youtu.be/Nu_6X04EGHk) [Tivoli](http://whc.unesco.org/en/list/907) / [Tibur]("https://www.bluffton.edu/~sullivanm/italy/tivoli/hadrian'svilla/hadrianintro.html") 
    * Hadrian's architectural incubator of sorts, heavily influenced by his travels, either brought there from abroad, or re-created there at Tivoli / Tibur by him and / or his architects
    * Had three building phases, and early, a middle, and a late; span the entire reign of Hadrian
    * Temple of Venus, late phase, 138 AD (compare to the Temple of Vesta at Tivoli, too (see above)), an exact replica of the Temple of Venus from the Greek island Knidos (round structure, low podium, Doric order, triglyphs and metopes, a statue of Venus in the center)
    * [Canopus]("http://web.mit.edu/course/21/21h.405/www/hadrian/Hadrian's%20Villa/Canopus.html"), 138 AD, an Egypt themed pool and a temple, heavily influenced by [Serapeum](http://en.wikipedia.org/wiki/Serapeum), also by the canal that lead to it and by the some sort "amusement park" that was there; has Egypt-themed sculptures, caryatids surrounding the pool, etc (the temple at the top of the pool was probably architectered by Hadrian himself, has a "pumpkin dome")
    * Large Baths, 125-133 AD, "springing" vaults(just the same way it was in Hadrian's market hall in Rome: groin vault that spring from a bracket, not from a column or a pilaster), stucco decoration,very large windows
    * Piazza d'Oro, early phase, 125-133 AD, was used as an audience hall, the octagonal entrance vestibule with a pumpkin vault / dome, open rectangular space, fairly traditional, surrounded by columns, with the audience aula on the other side, which combines traditional and innovative architecture of the time, like the Roman concrete construction with the Greek architecture: columns, annular vault, undulating walls, which were made of columns
    * Teatro Marittimmo, very early phase, 118-128 AD, round structure, has a bridge, brick-faced concrete walls, concrete domes with columns (Ionic capitals)
  * *Rome*: [Mausoleum of](http://en.wikipedia.org/wiki/Castel_Sant%27Angelo) [Hadrian](http://www.aviewoncities.com/rome/castelsantangelo.htm)
    * 135-138 AD
    * The walls and the watchtowers added later (during Vatican times)
    * As is the bridge, [Ponte Sant'Angelo](http://en.wikipedia.org/wiki/Ponte_Sant%27Angelo), designed by Bernini
    * Round structure (even though Titus before him was buried in his Arch, Trajan in his Column), probably wanted to be associated with Augustus (see above) and also to create a new tomb for a, a succession of dynasties
    * Podium (like Caecilia Metella tomb, too)
 
### Lecture 16: Ostia, the Port of Rome

  * *[Ostia](http://www.bluffton.edu/~sullivanm/italy/ostia/neptune.html)*: [Theater](http://www.vitruvius.be/theaterostia.htm)
    * Under Augustus Theater was added to the city of Ostia
    * Renovated in around 200 AD, expanded to hold 2500 spectators at that particular point
    * Renovated with brick-faced concrete (rather than stone-faced concrete in case of Augustian buildings)
    * Has a latrine, a really public one (benches that line walls, with one single drain that encircles the building)
    * Semi circular orchestra,
    * Scalloped stage building (scaenae)
    * Semi circular cavae, stone sits are placed on top of the concrete foundation
    * The theater is appended to a porticus (though, separated by the wall)
    * The porticus was also used for walks (to relax) during intermissions
  * *Ostia*: [Forum of the](https://en.wikipedia.org/wiki/Piazzale_delle_Corporazioni) [Corporations](https://www.bluffton.edu/~sullivanm/italy/ostia/corporations.html)
    * However, not the little usual shops (tabernae, cubicles, 61 of them) were surrounding the porticus but there was Pizzale delle Corporazioni, as it may be called today, there were import/export businesses (emporia) surrounded by concrete covered columns faced with brick, floors covered with black-and-white mosaics (sea imagery)
    * And also a small temple in the center of the porticus (typical Roman temple: rectangular shape, flat side and back walls, single staircase, facade orientation, free standing columns in the porch), dedicated to some god who has something to do with commerce
  * *Ostia*: [Baths of Neptune](http://www.bluffton.edu/~sullivanm/italy/ostia/neptune.html)
    * Concrete faced with brick (as everywhere else in Ostia)
    * Every floor is covered with black-and-white mosaics (as, again, almost everywhere else in Ostia)
    * Rectangular frigidarium (not a round one)
    * Surrounded by shops, again, as it should be in a strictly commercial town
  * *Ostia*: [Insula of Serapis](http://www.ostia-antica.org/regio3/10/10-3.htm)
    * 2 century AD
    * Brick-faced concrete (usually no stucco, no paint, etc, just brick speaks out for itself)
    * Multistored apartment house (5 floors)
  * *Ostia*: [Insula of Diana](http://www.ostia-antica.org/regio1/3/3-3.htm)
    * Four-storied partment building
    * Brick-faced concrete (again)
    * Has [moldings](http://en.wikipedia.org/wiki/Molding_(decorative))
    * No peristyle courts or hortus (no space for that); instead they had [lightwells](https://en.wikipedia.org/wiki/Lightwell)
    * Shops and thermopolia on the first floor (mosaic again)
  * *Ostia*: [Horrea Epagathiana](http://www.ostia-antica.org/regio1/8/8-3.htm)
    * 145-150 AD
    * Brick-faced concrete, even columns again (red for the column itself, yellow for the capitals)
    * Influence of Greece still may be seen (engaged columns supporting the pediment, pilasters, etc)
    * Vistas again
    * Niches (for the warehouse!)
  * *Ostia*: [Domus of Cupid and Psyche](http://www.ostia-antica.org/regio1/14/14-5.htm)
    * 300 AD
    * Long corridor is introduced (with a series of cubicles on the left side of it and a very large triclinium (which is definitely gaining its importance) on the right)
    * Grey granite columns
    * A fountain that was scalloped into the wall
  * *Fiumicino*: [Isola](https://en.wikipedia.org/wiki/Isola_Sacra) [Sacra](http://www.ostia-antica.org/valkvisuals/html/intro_verklaring.htm)
    * House type tombs, with a door and a couple of windows
    * Barrel vaulted
    * Even pediments and travertine facing
    * Arcosolia begin to appear (much larger niches where bodies are placed), covered with marble

## Week 9
### Lecture 17: The Baths of Caracalla and Other Second- and Third-Century[^4] [^5] Buildings in Rome 

  * *Rome*: [Tomb of](http://www.romeartlover.it/Vasi59b.htm) [Annia Regilla](http://en.wikipedia.org/wiki/Aspasia_Annia_Regilla)
    * 161 AD
    * Typical Roman temple look (not like a house): podium, facade orientation, deep porch, free-standing marble columns in that porch supporting a pediment
    * Also, quite unusual, two multisided columns on the left side in two niches in the wall
    * Done with concrete faced with exposed brick (exactly as in Ostia (see above))
    * Differently colored brick used to accentuate different elements
    * Also, as for pilasters, brick used to create the lower part of the acanthus leaves, and then stucco added for the curving part, and for some of the additional decoration, the flower and so on up above
    * Also, [meander](https://en.wikipedia.org/wiki/Meander_(art)) done in stucco
    * Elaborate windows decoration
  * *Rome*: [Tomb of the Valerii](http://www.coopculture.it/en/heritage.cfm?id=89)
    * 159 AD
    * Concrete faced with brick
    * Barrel vaulted
    * Painted stucco for the inside decoration (Tomb decoration Type A, references to the Third Style Roman Wall painting)
  * *Rome*: [Tomb of the Pancratii](http://digital.library.louisville.edu/cdm/singleitem/collection/vrc/id/1269/rec/8)
    * 169 AD
    * Groin vaulted
    * Again, painted stucco for the inside decoration, but a lot, lot more: walls are enlivened through architectonic means through the use of columns, niches, pediments, triangular pediments but also broken (split apart) triangular pediments (Tomb decoration Type B, references to the Forth Style Roman Wall painting: architectural cages, etc)
    * Not only niches for urns, [arcosolias](https://en.wikipedia.org/wiki/Arcosolium) for bodies but also sarcophagi themselves
    * Probably is related to [Orphism](https://en.wikipedia.org/wiki/Orphism_(religion))
  * *Vatican*: [Vatican](http://www.culturaltravelguide.com/saint-peters-basilica-vatican-necropolis) [Necropolis](https://en.wikipedia.org/wiki/Vatican_Necropolis)
    * [Tomb of the Caetenii](http://counterlightsrantsandblather1.blogspot.com/2009/11/ancient-saint-peters.html)
  * *Rome*: [Temple of Antoninus Pius](http://sights.seindal.dk/sight/175_Temple_of_Antoninus_and_Faustina.html) [and Faustina the Elder](http://en.wikipedia.org/wiki/Temple_of_Antoninus_and_Faustina)
    * 141 AD
    * Primarily by Antonius Pius to his wife Faustina, but later, after his death in 161 AD, rededicated to both of them
    * In general, best preserved temple to the emperor and the empress
    * In the 17th century reused as The Church of San Lorenzo in Miranda (E.g., the buttresses and the broken arcuated pediment were added)
    * Typical Roman temple: podium, facade orientation, deep porch, free-standing granite columns in that porch supporting a pediment
    * Tufa walls
  * *Rome*: [Arch of](http://en.wikipedia.org/wiki/Arch_of_Septimius_Severus) [Septimius Severus](http://sights.seindal.dk/sight/161_Arch_of_Septimius_Severus.html)
    * 203 AD
    * Placed diagonally to The Parthian Arch of Augustus
    * Triple opening
    * Heavily decorated with sculptures
    * Four panels depicting small figures in a number of tiers (think of the Column of Trajan)
  * *Rome*: [Baths](http://en.wikipedia.org/wiki/Baths_of_Caracalla) [of Caracalla](http://www.aviewoncities.com/rome/bathsofcaracalla.htm)
    * 212-212 AD
    * Largest baths ever
    * Concrete faced with brick
    * The natatio (a swimming pool) at very top in the north end
    * Below was the frigidarium with the tripple groin vault (gray granite columns with white marble capitals), buttressed by rooms with barrel vaults on either side
    * Below the rectangular vertically oriented tepidarium
    * Leading into the round caldarium (and, thus, all of those four on one axis: N -> F -> T -> C)
    * Alcoves in the caldiarium
    * Very large in scale (span of its dome is almost as large as span of the Pantheon's dome)
    * Decorated with Hercules statues and themes
    * Geometric mosaics

### Lecture 18: Honoring an Emperor's Roots in Roman North Africa

  * *[Leptis Magna](http://en.wikipedia.org/wiki/Leptis_Magna)*: [Augustan](https://www.temehu.com/Cities_sites/LeptisMagna.htm) [Market](http://whc.unesco.org/en/list/183)
    * 8 BC
    * Rectangular shape, colonnade all along the perimeter
    * Two octagon pavilions in the center (fluted pilasters)
    * Only temporary stalls between the columns around the pavilions and the columns in the portico
    * Open portico added under Tiberius (31-37 AD)
    * Thus, difference in the materials used: during the age of Augustus only local sandstone and limestone were used, and later, during the reign of Tiberius the portico columns were made out of gray stone (though, also local). I.e., no concrete whatsoever
  * *Leptis Magna*: Theater
    * Built on a hill of concrete (but still, very little concrete was used in Leptis Magna and in Roman North Africa in general)
    * Seats done from local stone (sandstone and limestone both for cavea and columns)
    * Stage building made up of three large niches that have columns screening the inside, following the curvature of those niches, and containing other architectural elements in the center of these niches
    * Also, had temple of [Ceres](http://en.wikipedia.org/wiki/Ceres_(mythology)) Augusta in the top of the cavea (so called Theater Temple)
  * *Leptis Magna*: [Hadrianic Baths](http://www.livius.org/le-lh/lepcis_magna/hadrianic_baths.html)
    * 126-127 AD
    * Quite unusual [palaestra](https://en.wikipedia.org/wiki/Palaestra): almost shaped like a hippodrome, has two curved ends (see Basilica Ulpia above), columns running around the center, two radiating apses
    * As usual, main bathing rooms are in the center of the structure, axially related to one another (natatio -> frigidarium -> tepidarium -> caldarium)
    * Caldarum has radiating niches
    * Other rooms symmetrically duplicated around this main axes on either side
    * Change in building materials: not only the local stone, as it was before (see above), but also marble from around the Empire
    * Possibly had groin vaults but done not with concrete
  * *Leptis Magna*: [Nymphaeum](http://www.livius.org/le-lh/lepcis_magna/nymphaeum.html)
    * 211 AD
    * Placed next to the palaestra of the Baths (see above)
    * Dates to the Severan period
    * Since then marble has found its way in Leptis Magna
    * One large central niche (had smaller niches for statuary) with a basin in front of it
    * Local stone for the wall constructions, Asia Minor marble for columns (two tiers of them)
  * *Leptis Magna*: [Severan Forum](http://www.vitruvius.be/severischebasiliek.htm)
    * 216 AD
    * Pink granite columns (granite from Egypt)
    * Local stone for the walls, imported marble for the doorways and pilasters
    * Striated capitals on the pilasters (similar to the Egyptian ones)
    * While at the bottom of the columns Roman acanthus leaves transforming to Lotus leaves (Egypt again)
    * One rectangular space surrounded by columns, with a temple put up against one of the short walls
      * Very tall podium
      * Plain back wall, facade orientation, deep porch, free-standing columns in that porch surrounding the temple from three sides
      * Single pyramid-shaped staircase (Egypt again)
      * Single cella
      * Local stone for the walls, marble for the columns
    * A series of usual shops and tabernae along one of the long walls
    * And also on the other short wall, opposite to the temple: wedge shaped (narrow on one end and wider on the other) series of shops that forms the transition between the Forum and the [Basilica](http://www.livius.org/le-lh/lepcis_magna/severan_basilica.html)
      * Again (see Hadrianic Baths above) has apses on either sides (think Basilica Ulpia in Rome)
      * Free standing columns with no structural purpose whatsoever
      * Corinthian capitals
      * Was two stored originally
      * Flat coffered ceiling
      * Decorative columns in the niches / apses with Ionic capitals
      * Decorated pilasters on either side
      * In the center of the niches two very tall columns on tall bases with Corinthian capitals with a lintel on top of those and then Griffins and then another lintel on the top
      * Intricately carved piers featuring the twelve labors of Hercules
  * *Leptis Magna*: [Arch of Septimius Severus](http://www.livius.org/le-lh/lepcis_magna/arch_severus.html)
    * 203 AD
    * [Tetrapylon](https://en.wikipedia.org/wiki/Tetrapylon)
    * A lot of figural scenes
    * Broken triangular pediment
  * *Leptis Magna*: [Hunting Baths](http://www.youtube.com/watch?v=YVlK_EeKCow)
    * Late 2 century, early 3 century AD
    * Private bath for a guild of hunters and animal collectors
    * Concrete structure
    * Octagon forecourt
    * Tepidarium with a segmented "pumpkin" dome
    * The shapes of vaults are also visible from the outside

## Week 10
### Lecture 19: Baroque Extravaganzas in the Eastern Provinces

  * *[Santa Maria Capua Vetere](http://en.wikipedia.org/wiki/Santa_Maria_Capua_Vetere)*: [La Conocchia](http://it.wikipedia.org/wiki/Mausoleo_La_Conocchia)
    * 1 century AD
    * Concrete faced with Opus Incertum (though, some [tile] brick has been used around the niches and as a molding, too, which leads to ambiguity on dating this tomb)
    * 3 stories: plain story, second story with the cylinders and [tholos](http://en.wikipedia.org/wiki/Tholos)
    * "Baroque characteristics": undulating facade (thus, "motion" introduced into monuments / buildings), decoration, "false" columns which look more like some cylinders (think the Tomb of the Baker), niches, pediments, tholos with the blind windows (thus the tholos is treated more as a decorative motif rather than something with a purpose)
    * Also, think of the contrast between the concavity of the curvature of the facade and the convexity of the outside of the above dome
  * *[Petra](http://en.wikipedia.org/wiki/Petra)*: [Dier](http://nabataea.net/deir.html)
    * Mid 2 century AD
    * Carved out of the living rock
    * Think of pictures one can see in Roman Second Style wall paintings (breaking the triangular pediment to reveal the tholos that lies inside, etc)
    * Again, uses the traditional vocabulary of architecture: the columns, the entablature above, pediments
    * Again, the undulating facade, an instilling motion across the facade done by contrasting a projecting bay with a receding bay
    * The tholos turned into a decorative motif among many, into a decoration on the surface, no "working" purpose whatsoever, has a niche, as others bays around it
    * Plain, undulating "Nabataean" capitals; metopes ornamented with "Nabataean" discs
    * The [finial](http://en.wikipedia.org/wiki/Finial) is also decorated with the Nabataean capitals
  * *Petra*: [Khazneh](http://en.wikipedia.org/wiki/Al_Khazneh)
    * Mid 2 century AD
    * Carved out of the living rock
    * Two stories
    * Again, "blind" tholos revealed by splitting the triangular pediment and treated purealy as a decorative motif (also, has a statue on a base, not a "real" statue but, again, carved out of the same rock)
    * "Real" temple facade on the first story (thus, much closer to Roman prototypes than Dier)
    * Corinthian order
    * Excess ornamentation
  * *[Mileatus](http://www.perseus.tufts.edu/hopper/artifact?name=Miletus&object=site)* [South Agora](http://www.fhw.gr/choros/miletus/en/notia_agora.php?mp=map4), [Market Gate](https://en.wikipedia.org/wiki/Market_Gate_of_Miletus) [of Miletus](http://www.fhw.gr/choros/miletus/en/pili_notias_agoras.php?mp=map4)
    * 160 AD
    * Entirely out of stone
    * "Baroque" facade
    * Triple opening down below, three blind windows on top
    * Composite capitals
    * In general, in Roman "baroque" columns are no longer used for structural purposes but mainly to decorate, and to enliven, and to add motion to the structure
    * Down below the lintels are straight, but up above they have combined full triangular pediments in the wings with a broken triangular pediment in the center (also make use of some kind of zigzag motif, which brings even a greater motion into the overall scheme)
  * *[Ephesus](http://en.wikipedia.org/wiki/Ephesus)*: [Library](http://en.wikipedia.org/wiki/Library_of_Celsus) [of Celsus](http://www.kusadasi.biz/ephesus/library.html)
    * The facade structure is very similar to the Market Gates of Miletus (see above)
    * Two tiers
    * A series of two columns junctions supporting a straight entablature above
    * On the second story, the columns straddle the space below, and, thus, the entablatures are placed in something like checkerboard order (think of architectural cages at the upper tier of the Fourth Style Roman Wall Painting)
    * Once again, series of projection, recession, projection, recession bays (creating motion)
  * *[Ephesus](http://www.kusadasi.biz/ephesus/hadrian.html)*: [Temple](http://www.sacred-destinations.com/turkey/ephesus-temple-of-hadrian) [of Hadrian](http://www.ephesus.us/ephesus/templeofhadrian.htm)
    * 120-130 AD
    * A kind of street-side shrine, not a real temple
    * Arcuated and straight lintels used together (see [architrave](https://en.wikipedia.org/wiki/Architrave))
    * Over-ornamentation
  * *[Sabratha](http://en.wikipedia.org/wiki/Sabratha)*: [Thea](http://whc.unesco.org/en/list/184)[tre](http://romeartlover.tripod.com/Sabrata2.html)
    * 200 AD
    * Local stone
    * Two tiers
    * Arches with with pilasters or columns in between then engaged into the walls
    * Stage building:
        * Three tiers
        * "Baroque" facade
        * Recession-projection bays
    * The whole stage is overdecorated with sculptures
  * *[Baalbek](http://en.wikipedia.org/wiki/Baalbek)*: [Sanctuary](http://sacredsites.com/middle_east/lebanon/baalbek.html) [of Jupiter Heliopolitanus](http://www.livius.org/ba-bd/baalbek/baalbek_t_jupiter_shrine.html)
    * Mid 1 century AD (much of the whole structure - sometime around mid 2 century AD, [propylaea](https://en.wikipedia.org/wiki/Propylaea) and the hexagonal court added sometime in the 3 century AD)
    * Grand entrance way with a single staircase, facade orientation with an arcuated lintel contained within a pediment
    * Hexagonal court
    * From there to great rectangular space surrounded by columns, with a very large altar to Jupiter in the center
    * As for the temple itself, it's a usual Roman temple, with the very tall podium (44 ft tall), single staircase, facade oriented, deep porch, free-standing columns in that porch (10 columns in the front, 19 on the sides, made of honey colored local limestone, 65 ft tall)
    * Also, the temple of Bacchus (see below) right outside the walls and the Temple of Venus to the left of the cpmplex in the front
  * *Baalbek*: [Temple](http://en.wikipedia.org/wiki/Temple_of_Bacchus) [of Bacchus](http://www.livius.org/ba-bd/baalbek/baalbek_t_bacchus.html)
    * Mid 2 century AD
    * Has his own small courtyard
    * Single staircase facade orientation, deep porch, free standing columns in the porch (and also a peripteral colonnade, but not a doesn't have a peripteral staircase), single cella
    * Once again, overly decorative
    * Corinthian columns
    * Niches on two stories with arcuated pediments and the triangular pediments
    * Flat coffered ceiling
    * [Adyton](http://en.wikipedia.org/wiki/Adyton) at the opposite short wall
  * *Baalbek*: [Temple](http://www.livius.org/ba-bd/baalbek/baalbek_t_venus.html) [of Venus](http://atheism.about.com/od/religiousplaces/ig/Baalbek-Temples-Lebanon/Baalbek-Temple-Venus.htm)
    * Early to mid 3 century AD
    * Made entirely out of local stone
    * Round structure with a facade orientation (single staircase, deep porch, free standing columns in that porch) and a round cella (think of Pantheon: the traditional facade and the revolutionary round body)
    * Peripteral colonade
    * Scalopped outside (both the podium and in the area of the entablature above the columns)
    * Niches with statues around the temple in the walls

### Lecture 20: The Rebirth of Athens

  * *Athens*: [Erech](http://en.wikipedia.org/wiki/Erechtheion)[theion](http://www.sacred-destinations.com/greece/athens-erechtheion-temple)
    * 421-406 BC
    * [Poros](https://www.wordnik.com/words/poros) (a limestone from a specific place) for the foundation, pentelic marble for the walls and the columns (which is blindingly white), eleusinian (light blue) marble for the frieze
    * Ionic order
    * Porch of Maidens (Caryatid porch)
  * *[Eleusis](http://en.wikipedia.org/wiki/Eleusis)*: [Inner](http://www.gettyimages.com/detail/news-photo/engraving-of-inner-propylaia-the-entrance-to-the-acropolis-news-photo/466672341) [Propylaia](http://en.wikipedia.org/wiki/Propylaea)
    * 50 BC (late Republican period in Roman history)
    * Zoomorphic columns (see the Forum of Augustus, which also had these capitals, pegasi growing out of the acanthus leaves)
    * On the other side of the gate caryatids replace the columns with their zoomorphic capitals
    * Triangular pediment
    * Triglyphs and metopes have representations of sheaves of wheat and [cistae](http://en.wikipedia.org/wiki/Cista) (or baskets)
  * *Athens*: [Temple of Roma and Augustus](http://city-of-athens.com/the-temple-of-rome-and-augustus-in-the-acropolis-of-athens/archaeological-sites/783.html) // *(placed in the back left corner of the of the Parthenon)*
    * 19-17 BC
    * Made out of stone
    * Based on a square foundation
    * Round structure
    * 9 Ionic columns (which closely resemble the capitals of Erechtheion)
    * Curved entablature, curved architrave
    * Sloping, stone roof
  * *Athens*: [Monument of Agrippa](http://www.my-favourite-planet.de/english/europe/greece/attica/athens/acropolis/acropolis-photos-01-009.html) [and Propylaia](http://www.athensinfoguide.com/wtsacropwest.htm)
    * First made by [Eumenes II](https://en.wikipedia.org/wiki/Eumenes_II) in 178 BC to honor himself, later transformed by [Marcus Agrippa](https://en.wikipedia.org/wiki/Eumenes_II) in the 1 century BC to honor himself
    * Made of Hymettian marble
  * *Athens*: [Odeion](http://www.agathe.gr/guide/odeion_of_agrippa.html) [of Agrippa](http://en.wikipedia.org/wiki/Odeon_of_Agrippa)
    * Placed in [Agora](https://en.wikipedia.org/wiki/Ancient_Agora_of_Athens), surrounded by [Plaka](https://en.wikipedia.org/wiki/Plaka)
    * 15 BC
    * In contrast to Roman Agora (and, obviously, a Roman Forum) Greek Agoras doesn't have that central focus of having a single temple on one end dominating the space in front of it
    * Very closely based on the Odeon of Pompeii (which was built in 80-70 BC, placed next to the Theatre of Pompeii):
      * The semi-circular orchestra, the cavea divided into cunae, stage front pilasters, tall pilasters on some of the walls, also tall pilasters on the outside of this structure
      * An open [stoa](https://en.wikipedia.org/wiki/Stoa) in the back, two sets of columns, an inner row and an outer row
      * Two entrances, for spectators through the porch / stoa in the back (the southern end), and for musicians and visiting dignitaries in the front (the northern end), which was made of a small temple front with a pediment and Doric columns
      * As all [Odea](https://en.wikipedia.org/wiki/Odeon_(building)), was roofed for the acoustics
    * Corinthian order for the outer columns
    * Egyptian order for the inner columns: lotus leaves growing out of the canvas leaves (inspired by capitals in Egypt, used later at Leptis Magna, see the Sempron Forum above)
  * *Athens*: [Roman](http://www.eie.gr/archaeologia/En/chapter_more_5.aspx) [Agora](http://en.wikipedia.org/wiki/Roman_Agora)
    * Started in the Caesarean period and completed by Augustus
    * Usual large open rectangular space with a colonnade (stoas) around it
    * Opposite to Roman forums, doesn't have a temple on one of short ends (and not as long, too)
    * Tabernae or shops at the upper most part (eastern part)
    * Elaborate entrance way on the western end, [Gate of Athena Archegetis](http://en.wikipedia.org/wiki/Gate_of_Athena_Archegetis)
      * Doric coulmns
      * Triangular pediment (and never a broken pediment)
    * Also an entrance on the eastern end as well
    * Horologion of Andronicus, or The Tower of Winds, an octagonal building at the eastern end
  * *Athens*: [Tower](http://www.sailingissues.com/yachting-guide/tower-of-the-winds.html) [of the Winds](http://en.wikipedia.org/wiki/Tower_of_the_Winds)
    * second half of the 1 century BC (though John Camp dates it to 150-125 BC)
    * Some kind of a clock tower (sun dial on every side of the monument)
    * Two porches that had temple fronts with triangular pediments
    * Egyptian order
    * There was also a staircase that surrounded the structure
  * *Athens*: [Library](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/greece/architecture/library.html) [of Hadrian](http://en.wikipedia.org/wiki/Hadrian%27s_Library)
    * 132 AD
    * Large rectangular open space surrounded by columns
    * Projecting columns on that facade (thus creating a kind of undulating walls)
    * Walls made out of pentelic marble, columns out of slightly greenish tinged marble from Karystos
    * Planted with greenery
    * Pool in the center
    * Series of niches-circular and rectangular niches in the walls
    * Library itself in the uppermost part of the structure
    * Other rooms in the uppermost part forming a kind of wing on either side 
    * Think Templum Pacis (wich was kind of museum) in Rome (see above)
  * *Athens*: [Olympieion](http://en.wikipedia.org/wiki/Temple_of_Olympian_Zeus,_Athens)
    * Began by [Peisistratos](https://en.wikipedia.org/wiki/Peisistratos) in [Archaic period](https://en.wikipedia.org/wiki/Archaic_Greece) as a Doric temple
    * Stopped in 510 BC
    * Continued by [Antiochus IV Epiphanes](https://en.wikipedia.org/wiki/Antiochus_IV_Epiphanes) in [Hellenistic period](https://en.wikipedia.org/wiki/Hellenistic_period) in 174 BC
    * Architected then by a Roman architect by the name of Cossutius as a Corinthian temple; had finished it up to the architrave
    * Stopped again in 164 with the death of Antiochus
    * After Sulla sacked Athens in 86 BC, the columns were taken to Rome to be used in Sulla's renovation of the Temple of Jupiter Optimus Maximus Capitolinus aka OMC (see above), introducing by that the Corinthian order to Rome
    * Hadrian finishes it (according to Cossutius's plan) in 131-132 AD 
    * Under Hadrian statues of himself and Zeus were put in the structure 
    * As well as many statues of Hadrian outside the temple, in the courtyard, around the structure
    * Does not have a facade orientation
    * Has two entrance ways (one with the statue of Hadrian, one with the statue of Zeus)
    * Has columns that encircle the entire monument, a peripteral colonnade
    * And also has a staircase that goes all the way around the structure, too
  * *Athens*: [Arch of Hadrian](http://en.wikipedia.org/wiki/Arch_of_Hadrian_(Athens))
    * 131-132 AD
    * Single arcuate, arcuation in the center
    * Corinthian order
    * No-split triangular pediment in the second story
    * Had a marble slab that was located in the center of that pediment (or [aedicula](http://en.wikipedia.org/wiki/Aedicula))
    * Had statues of Theseus and Hadrian on both sides of this, too
  * *Athens*: [Monument](http://city-of-athens.com/philopappos-monument-filopappou/athens-sights/341.html) [of Philopappos](http://en.wikipedia.org/wiki/Philopappos_Monument)
    * 116-117 AD (thus, dates to the time of Trajan)
    * A mausoleum and monument dedicated to Gaius Julius Antiochus Epiphanes Philopappos (65-116 AD) (son of [Gaius Julius Archelaus Antiochus Epiphanes](https://en.wikipedia.org/wiki/Gaius_Julius_Archelaus_Antiochus_Epiphanes)
    * A tower tomb with a plain base and a curved, second story that has the scene of Philopappos in a chariot, in the time of his consular procession
    * Made out of pentellic marble, carved
    * Incorporates both Roman and Anatolian elements, even though it was located in Athens (the tomb is located on the hill and the rayed crown is displayed on the portrait of Philopappos in the chariot)

## Week 11
### Lecture 21: Mini Romes on the Western Frontier

  * *[Aosta](http://en.wikipedia.org/wiki/Aosta)*:
    * City founded in 24 BC (Augusto Pretoria)
    * Absolutely typical [castrum](https://en.wikipedia.org/wiki/Castra) plan, ideal rectangular, two main streets, cardo and decumanus meet in the center, in the Forum
    * Surrounded by walls, have a series of gates
        * Single arcuated bay in the center, flanked on either side by wide pedestals that have a set of double columns
        * Made out of local stone (which is a characteristic of so much of provincial Roman architecture)
        * The ancient attic is gone and replaced with a modern roof
        * Corinthian order with triglyphs and metopes (which is not quite the thing that usually goes with the Corinthian order, instead they commonly accompany the Doric order; thus, a pretty eccentric arch)
  * *[Arles](http://en.wikipedia.org/wiki/Arles)*: Forum
    * Much of the Forum is underground today
    * Had [cryptoporticus](http://www.perseus.tufts.edu/hopper/artifact?name=Cryptoporticus,+Arles&object=building), an underground storage area around the colonnade (think of cryptoportici at sanctuaries, like the Sanctuary of Hercules at Tivoli or the Sanctuary of Jupiter Anxur at Terracina)
    * The famous [Arles bust](https://en.wikipedia.org/wiki/Arles_bust) was found there
  * *[Orange](http://en.wikipedia.org/wiki/Orange,_Vaucluse)*: [Roman](http://en.wikipedia.org/wiki/Th%C3%A9%C3%A2tre_antique_d%27Orange) [Theatre](http://www.theatre-antique.com/en/discovering-site/roman-theatre-orange)
    * Late 1 century BC, early 1 century AD
    * The typical example of Roman theater design
    * Made on a natural hillside
    * Three stories, columns on all of them
  * *Orange*: [Arch of](http://www.mibba.com/Articles/History/2106/The-History-of-the-Romans-Orange-Arch/) [Tiberius](http://en.wikipedia.org/wiki/Triumphal_Arch_of_Orange)
    * During the reign of Augustus, 27 BC - 14 AD? Tiberius time, 14-37 AD, may be 25 AD? Much later, say 200 AD?
    * Triple-bayed arch with a central arch, also two smaller ones on either side
    * Corinthian columns on tall bases in between them
    * Triangular pediment
    * Completely covered with decoration (piles of arms and armor from the enemy, etc)
    * Think of the Arch of Septimius Severus
  * *[Vienne](http://en.wikipedia.org/wiki/Vienne)*: [Temple of Augustus and Roma](http://www.livius.org/place/vienna-vienne/) [(or Livia)](https://en.wikipedia.org/wiki/Imperial_cult_(ancient_Rome))
    * 14 AD
    * Was part of a complex (some kind of a Forum)
    * Was also used as a marketplace and as a museum in later pariods
    * Made of local limestone but also marble
    * Pretty shallow cella (think the Temple of Mars Ultor in Rome)
    * Freestanding columns columns all the way up to the back (again, think the Temple of Mars Ultor in Rome)
    * Because of the above, today we call such a plan a [temple with alae (or wings)](http://www.vitruvius.be/boek4h7.htm)
  * *[Nimes](http://en.wikipedia.org/wiki/N%C3%AEmes)*: [Maison](http://www.maisoncarree.eu/en/) [Carrée](http://en.wikipedia.org/wiki/Maison_Carr%C3%A9e) // *(also, visit [Carrée](http://www.carreartmusee.com/) [d’Art](http://en.wikipedia.org/wiki/Carr%C3%A9_d%27Art) at [Nimes](http://www.buffaloah.com/a/virtual/fr/nimes/), per contra, and also notice obvious similiarities)*
    * 16 BC (dedicated in 5 AD?)
    * Was part of a complex (some kind of a Forum)
    * Made of local limestone (opus quandratum) and marble
    * Think of the Temple of Portunus
    * Even though this is a Corinthian temple, not Ionic, as the Temple of Portunus
    * As for the capitals, note the spiral volutes growing out of the acanthus leaves down below; extremely close to the ones of the Temple of Mars Ultor (dedicated in 2 BC) that it's even suggested that they were done by the same workshop
    * Also has pseudoperipteral colonnade (including the back wall) 
  * *Nimes*: [Aqueduct System](http://www.athenapub.com/gardmap1.htm) // *"aqueducts are like bridges through the time"*
    * As one may say, they have transformed engineering into architecture: note not only mathematical formulas but also aesthetics 
    * [Pont](http://www.pontdugard.fr/en) [du Gard](http://en.wikipedia.org/wiki/Pont_du_Gard)
        * Patroned by Marcus Agrippa, 64-12 BC (or was the Aqueduct built much later after him, in 40-60 AD?)
        * Ashlar masonry (local stone)
        * Terracotta pipes are placed into the bridge itself
  * *Nimes*: [Temple](http://ratha-historyarch.blogspot.com/2011/09/temple-of-diana-in-nimes.html) [of](http://www.ot-nimes.fr/index.php?id=94) [Diana](https://commons.wikimedia.org/wiki/Category:Temple_of_Diana_in_Nîmes)
    * 100-130 AD
    * A fountain incorporated into a temple
    * A barrel-vaulted central chamber with side aisles that are also barrel-vaulted (no concrete though, local stone only)
    * Series of niches along the wall
  * *[Segovia](http://en.wikipedia.org/wiki/Segovia) (near Madrid)*: [Aqueduct](http://en.wikipedia.org/wiki/Aqueduct_of_Segovia), [etc](http://whc.unesco.org/en/list/311)
    * 1 AD? 2 AD?
    * Made of local stone
    * For the most part a two tiered aqueduct system (though the tiers are different in height)
    * Also, much more attenuated arches for the lower story
  * *[La Turbie](http://en.wikipedia.org/wiki/La_Turbie)*: [Tropaeum](http://www.livius.org/la-ld/la_turbie/la_turbie.html) [Augusti](http://en.wikipedia.org/wiki/Tropaeum_Alpium)
    * 7-6 BC
    * Gallo-Roman version of a concrete construction with stone facing (not opus incertum though, but is called petit appareil)
    * Very high podium
    * Round structure with columns encircling it
    * Pyramidal element at the top
    * Crowning statue at the very apex (of funerary architecture from the age of Augustus)
  * *[Pula](https://en.wikipedia.org/wiki/Pula,_Croatia)*: [Arch of the](http://romeartlover.tripod.com/Pola.html) [Sergii](http://en.wikipedia.org/wiki/Arch_of_the_Sergii)
    * End of the 1 century BC
    * Put by a woman named Salvia Postuma Segia in memory of three male family members that have dies in military operations
    * Thus, probably had three statues at the top
    * Local stone
    * Single bay
    * Two columns on a shared base, Corinthian order
    * Also, strangely, cupids carrying garlands
    * Also, a usual chariot scene ("race of life")
  * *[St Remy](http://en.wikipedia.org/wiki/Saint-R%C3%A9my-de-Provence)*: [Mausoleum](http://www.tripadvisor.com/LocationPhotoDirectLink-g187256-d230715-i78125893-Site_Archeologique_de_Glanum-Saint_Remy_de_Provence_Bouches_du_Rhone_Prove.html) [of the Julii and Arch](http://goeurope.about.com/od/provence/ss/saint-remy-day_3.htm)
    * 30-20 BC
    * 'Julii' here possibly stands for veterans of Julius Caesars' army
    * Stepped base
    * [Socle](http://en.wikipedia.org/wiki/Socle_(architecture)) with sculptural figural frieze
    * [Quadrifrons](http://en.wikipedia.org/wiki/Quadrifrons)
    * Corinthian order
    * At the very apex, a tholos that has two statues inside
  * *[Les Baux](http://en.wikipedia.org/wiki/Les_Baux-de-Provence) [de Provence](http://www.lesbauxdeprovence.com/en)*
  * *[Glanum](http://en.wikipedia.org/wiki/Glanum)*: [Stelae](http://glanum.monuments-nationaux.fr/en/)
  * *[Aquileia](http://en.wikipedia.org/wiki/Aquileia)*: [Tholos tomb](http://en.wikipedia.org/wiki/Beehive_tomb)
    * Early Augustan period, something like 25-20 BC
    * Some lions statues in front to guard the tomb
    * Large base with a sculptural decoration
    * Tholos at the top with a statue inside
    * Also, a pyramidal roof with a decorative element at the uppermost part
  * *[Aquileia](http://en.wikipedia.org/wiki/Aquileia)*: [Altar tombs](http://romeartlover.tripod.com/Aquileia3.html)
    * Early Augustan period, too
    * Very much of some modern graveyards
    * Stone fence that encircles and protects their plot

### Lecture 22: The Crisis of the Third Century[^6] and The Tetrarchic Renaissance[^7]

  * *Rome*: [Servian](http://en.wikipedia.org/wiki/Servian_Wall) [Walls](http://www.jeffbondono.com/TouristInRome/ServianWall.html)
    * 378 BC
    * Cut stone ashlar blocks
  * *Rome*: [Aurelian](http://en.wikipedia.org/wiki/Aurelian_Walls) [Walls](http://www.aviewoncities.com/rome/aurelianwall.htm)
    * Started in 270 AD, not finished by Aurelian but only dedicated by Probus right after Aurelian's death in 275 AD
    * 12 mile circuit around the city of Rome
    * Originally 25 and 1/2 ft tall
    * Had 18 major gateways
    * Concrete faced with brick (many bricks were reused from earlier periods)
    * [Porta Appia](http://roma.andreapollett.com/S4/aurel32.htm)
        * 275 AD
        * Later restored by Byzantine emperors Honorius and Arcadius (401-402 AD)
  * *Rome*: [Curia](http://dlib.etc.ucla.edu/projects/Forum/reconstructions/CuriaIulia_1) [Julia](http://en.wikipedia.org/wiki/Curia_Julia)
    * Previous Senate building destroyed by fire in 283 AD, this new one begun in 284, completed in 305
    * Concrete faced with exposed brick
    * Marble revetment (a façade of stone slabs or decorated ceramic plaques used as the outer facing layer of a wall) at the bottom part of the wall behind a series of columns (portico)
    * Three windows with curved top at the second story
    * Triangular pediment up above
    * Flat coffered ceiling
    * Was turned to a church in 7 century AD, restored first in 12 century, then in 16 century. Later transformed into Baroque church by Martino Longhi the Younger
  * *Rome*: [Baths](http://www.aviewoncities.com/rome/bathsofdiocletian.htm) [of Diocletian](http://en.wikipedia.org/wiki/Baths_of_Diocletian)
    * 298-306 AD
    * Like the other imperial baths have the central bathing rooms in the center in axial relationship to one another:
        * Natatio, or the swimming pool with the scalloped wall next to frigidarium
        * Frigidarium, the cold room, with a triple groin vaulted ceiling
        * Tepidarium, the warm room, a small circular structure with radiating arms that gives it a cross shape
        * Caldarium, the hot room, not round with alcoves any more but more rectangular shape, similar to frigidarium, aslo triple groin vault, with radiating alcoves
    * Also, the outer precinct has the large hemicycle for the perfomances
    * Were reused later for a variety of buildings (see below)
  * *Rome*: [Santa Maria](http://www.sacred-destinations.com/italy/rome-santa-maria-degli-angeli) [degli Angeli](http://en.wikipedia.org/wiki/Santa_Maria_degli_Angeli_e_dei_Martiri)
    * Redesigned by Michelangelo
    * The bottom alcove of the caldarium as the curved facade of the church
    * Tepidarium as the vestibule
    * Frigidarium as the main space of the church
    * Granite columns still there
    * Pilasters added in 1749
  * *[Split](http://en.wikipedia.org/wiki/Split,_Croatia)*: [Palace](http://whc.unesco.org/en/list/97) [of Diocletian](http://en.wikipedia.org/wiki/Diocletian%27s_Palace)
    * 300-305 AD
    * In the form of a [Roman military camp](http://en.wikipedia.org/wiki/Castra)
    * Porta Aurea, or the northern gate of the palace
        * Three tiers
        * Rectangular entrance way
        * Lintel window-like with grates above
        * Niches on either side with arcuated pediments
        * In the upper tier a series of columns on brackets that support arcades (think the Forum at Leptis Maga, think late residential architecture in Ostia, of aqueducts)
    * Has tomb inside (never seen a tomb inside a city before), octagonal in form
        * Deep porch on the front, free standing columns surrounding the structure, single staircase
        * Two tiers
        * Extremely ornated interior (in contrast to simple forms and shapes of the rest of the palace)
  * *[Piazza Armerina](http://en.wikipedia.org/wiki/Piazza_Armerina)*: [Palace of Maximian](http://en.wikipedia.org/wiki/Villa_Romana_del_Casale)
    * Early 4 century AD
    * Placed in the remote town in south-central Sicily
    * Absolutely different in form and shape to the Palace of Diocletian (more similar to the Villa of Hadrian at Tivoli)
    * Famous "bikini girls" mosaics
  * *[Salonica](http://en.wikipedia.org/wiki/Thessaloniki)*: [Palace](http://www.enjoythessaloniki.com/visit/ancient-monuments/galerius-palace/) [of Galerius](http://en.wikipedia.org/wiki/Arch_of_Galerius_and_Rotunda)
    * 297-305 AD
    * Had a hippodrome (think of Domitian's Palace on the Palatine Hill)
    * Also, had an octagonal room with alcoves (again, think of Domitian's Palace)
    * But also includes a tomb (concrete, faced with brick, round with radiating rectangular alcoves, utilizes windows rather than an [oculus](http://en.wikipedia.org/wiki/Oculus), later was turned into a church)
    * Two main streets intersects almost in the center of the palace with an Arch placed there
      * *Salonica*: [Arch](http://www.thessaloniki.travel/index.php/en/component/content/article?id=22) [of Galerius](http://en.wikipedia.org/wiki/Arch_of_Galerius_and_Rotunda)
        * Four-sided, triple bay (single central large bay, two smaller bays on either side)
        * Highly decorated panels
  * *Rome*: [Villa](http://rometour.org/villa-circus-maxentius-and-romulus-mausoleum-tomb-or-circus-caracalla-appia-antica.html) [of Maxentius](http://en.wikipedia.org/wiki/Circus_of_Maxentius)
    * Early 4 century AD
    * Also had hippodrome (was indeed used as a circus, was built to hold 15,000 spectators)
    * Also, as others above, had a tomb, which probably looked something like mini-Pantheon
  * *Rome, [Via Praenestina](http://en.wikipedia.org/wiki/Via_Praenestina)*: [Tor](http://www.romeartlover.it/Preneste.html) [de’Schiavi](http://en.wikipedia.org/wiki/Villa_Gordiani)
    * Around 300 AD
    * Another mini-Pantheon structure
    * Single staircase facade, facade orientation
    * Deep rectangular porch, free standing columns in that porch, triangular pediment (or the arcuated lintel?)
    * Round windows instead of oculus
    * Rectangular alcoves inside

## Week 12
### Lecture 23: Rome of Constantine[^8] and the New Empire[^9]

  * *Rome*: [Baths](http://roma.andreapollett.com/S6/roma2-03e.htm) [of Constantine](http://en.wikipedia.org/wiki/Baths_of_Constantine_(Rome))
    * 320 AD
    * Were located at The Quirinal Hill (think Trajan) but no longer preserved
    * Also, as many other baths, included the great hemicycle as a precinct that surrounded the bath the bath block
    * Up from the hemicycle go:
        * Caldarium, circular, with three radiating alcoves, screened from the outer space with columns (think Caracalla's baths), with no oculus but windows instead
        * Tepidarium, with a series of four lobes that have been expanded the space almost making it an oval
        * Frigidarium, rectangular, triple groin vault
        * Natatio, rectangular
  * *Trier*: [Porta](http://en.wikipedia.org/wiki/Porta_Nigra) [Nigra](http://www.sacred-destinations.com/germany/trier-porta-nigra)
    * The wall started in 275-276 AD, the gate itself added in the early 4 century AD
    * Made out of cut stone (not smoothed over as the stone of the Colosseum or the theater of Marcellus; think of "rusticated masonry" of Porta Maggiore in particular and Claudius architecture in general), not concrete/brick
    * Think of Porta Maggiore and especially of Porta Appia (before the renovation in the 5 century AD)
    * Two arcuated entrance ways
    * Two round towers (4 tiers)
    * [Blind] arcuated windows, columns between them
  * *Trier*: [Aula](http://en.wikipedia.org/wiki/Aula_Palatina) [Palatina](http://www.sacred-destinations.com/germany/trier-aula-palatina)
    * 300-310 AD
    * Made out of solid brick
    * Is part of the Palace of Constantius Chlorus, completed by Constantine (the palace had a large bath building, too)
    * Two stories, railings on them (no longer present)
    * A great open rectangular space with an apse on one of short ends, with all attention drawn toward that apse
    * No columns inside the cella (think Curia Julia)
    * However, there were columns outside the Basilica
    * A transverse corridor on the other short end (or a vestibule, usually called [narthex](http://en.wikipedia.org/wiki/Narthex)
    * Arcuated windows
    * Some kind of simplified projecting elements, pilasters with no capitals and no bases
    * Flat, coffered ceiling
  * *Rome*: [Santa](http://en.wikipedia.org/wiki/Santa_Sabina) [Sabina](http://www.sacred-destinations.com/italy/rome-santa-sabina)
    * 425 AD
    * Extremely close in form and shape to the Aula Palatina
  * *Rome*: [Temple](https://en.wikipedia.org/wiki/Temple_of_Minerva_Medica_(nymphaeum)) [of Minerva Medica](http://penelope.uchicago.edu/Thayer/E/Gazetteer/Places/Europe/Italy/Lazio/Roma/Rome/Minerva_Medica/home.html)
    * Early 4 century AD
    * Was a pavilion in the Licinian Gardens
    * Concrete faced with brick
    * Decagonal (ten-sided) structure: nine radiating apses screened by columns with the triple arcuation and an entrance
    * The apse located directly across the entrance way is a bit larger (which gives a certain longitudinal axis)
    * Narthex as a vestibule with aoses on either end
    * No ouclus again but arcuated (round headed) windows
  * *Ravenna*: [San](http://en.wikipedia.org/wiki/Basilica_of_San_Vitale) [Vitale](http://employees.oneonta.edu/farberas/arth/arth212/san_vitale.html)
    * 547 AD
    * See how heavily influenced it is by the aforementioned examples:
        * The interest in geometry, the accentuation of simple forms
        * Arcuated windows
        * Octagonal structure with radiating apses screened by columns with the triple arcuation
        * Narthex
  * * *Rome*: [Basilica](http://en.wikipedia.org/wiki/Basilica_of_Maxentius) [Nova](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/romanforum/basilica.html)
    * Was begun by Maxentius in 306 AD, completed by Constantine in 312 AD
    * Concrete, faced with brick
    * Rectangular structure
    * Dematerializing the wall at its best: extremely large round-headed windows on both stories
    * Also, three giant barrel vaults
    * Think of close resemblance to the frigidarium, too
    * Constantine's changed the orientation, from the longitudinal axis, from the east-west to north-south, and to place the entrance not on one of the short sides, but instead on one of the long sides
    * Thus, high podium, facade orientation, four free-standing porphyry columns in the porch
    * Sitting statue of Constantine in the apse at the short end to the left from the entrance and, probably, also another standing statue of him and his lieutenants right opposite to the entrance
  * *Rome*: [Arch](http://en.wikipedia.org/wiki/Arch_of_Constantine) [of Constantine](http://www.rome101.com/Topics/ArchConstantine/)
    * 312-315 AD
    * A triple bayed arch (think of Arch of Septimius Severus in the Roman Forum)
    * Also, covered with sculpture (think again)
    * However, some sculptural works were taken from the earlier monuments of Trajan, Hadrian and Marcus
Aurelius – and then replaced heads of of Trajan, Hadrian and Marcus Aurelius with his own portrait
  * *[Constantinople](http://en.wikipedia.org/wiki/Constantinople)*: [Hippodrome](http://en.wikipedia.org/wiki/Hippodrome_of_Constantinople) [of](http://www.livius.org/cn-cs/constantinople/constantinople_hippodrome_1.html) [Constantinople](http://www.istanbultrails.com/2008/05/the-hippodrome-of-constantinople/)
  * *[Constantinople](http://www.roman-empire.net/constant/constant-index.html)*: [Valens](http://en.wikipedia.org/wiki/Valens_Aqueduct) [Aqueduct](http://istanbul.for91days.com/2013/04/25/the-aqueduct-of-valens/)
  * *Constantinople*: [Hagia](http://en.wikipedia.org/wiki/Hagia_Sophia) [Sophia](http://www.sacred-destinations.com/turkey/istanbul-hagia-sophia)

## Appendix
### Paper Topics: [Discovering the Roman Provinces and Designing a Roman City](http://oyc.yale.edu/history-art/hsar-252/paper-topics)

  * *[Corinth](http://en.wikipedia.org/wiki/Ancient_Corinth)*:
    * What was the city in the Archaic Greek Period?
    * How did Romans remake it?
    * How did the blend go?
    * What was the purpose of [The](https://en.wikipedia.org/wiki/Corinth_Canal) [Canal](http://www.amusingplanet.com/2012/03/tightest-squeeze-corinth-canal-greece.html)? How did it evolve?
  * *[Corinth](http://romeartlover.tripod.com/Corinto2.html)*: [Roman]("http://corinth.ascsa.net/id/corinth/monument/roman%20bath%20(great%20bath%20on%20the%20lechaion%20road)&t=monument&v=list") [Baths](http://boards.cruisecritic.com/showthread.php?t=1399986)
    * Which are the differences from the real Roman Baths? Why did they took place? Were any consequences?
    * Why differences in the material (cut stone instead of concrete), what kind of impact did that cause? Is it typical for the Roman provinces?
    * What is the plan? How similar to the usual Roman baths plan? Axial symmetry? Layout? Gender separation?
  * *[Ephesus](http://www.ephesus.us/)*: [Temple](http://en.wikipedia.org/wiki/Temple_of_Artemis) [of](http://www.sourcememory.net/art/anadolu/upis.html) [Artemis](http://www.livius.org/religion/artemis-of-ephesus/)
    * Note:
        * Eight Ionic columns
        * Pedimental decoration
        * Spread for the cult statue of Artemis in the center:
            * "One of Artemis' characteristics is that she protects fertility. This may be symbolized by the spherical objects that cover the lower part of her chest, but the common assumption that they are female breasts is incorrect. They probably represent the testicles of a bull, although they may also be gourds, which were known in Asia as fertility symbols for centuries. Artemis' robe is always decorated with lions, leopards, goats, griffins, and bulls, which represent Artemis' title of Lady of the Animals."
            * "A Roman-era copy of the original statue of Artemis Ephesia, destroyed in 401 when John Chrystostom led a mob and attacked the temple. The multiple breasts have also been described as eggs or fruits, especially figs (compare the crescent of grapes above it). Gerard Seiterle's theory that they were sacrificed bull testicles became popular among those who wanted to contradict the obvious breast symbolism. They argue that bull testicles were sacrificed to the Anatolian goddess Kybele, whose castrati priests offered up their own. By the sixth century, Artemis had her own eunuch-priest, the megabysos."
            * Further development and use of that symbolism (Think of the [fountain]("https://commons.wikimedia.org/wiki/File:Fontana_di_Diana_Efesina-Tivoli,_Villa_d'Este.jpg") at [Villa d'Este](http://en.wikipedia.org/wiki/Villa_d%27Este))
  * *[Ephesus](http://en.wikipedia.org/wiki/Ephesus)*: [Library](https://en.wikipedia.org/wiki/Library_of_Celsus) [of Celsus](http://www.ancient.eu/Celsus_Library/)
    * Why marble? How was it affected by geography? culture? history? (Also, the Arch of Augustus, the Temple of Hadrian (compare the latter to the Pantheon, which was built at the almost the same time: size, materials, architecture, levels of decoration, etc; besides, note the introduction of the straight-arcuated-straight lintel - and how later it was brought to Rome))
    * How was it organised?
    * How many tiers inside, how many outside?
    * How the motion was introduced? 
    * Note how are the columns in the second tier placed (think of the Roman Forth Style Wall Painting), does it reinforces motion, or, on the contrary, slows it down?
    * Introduction of a tomb in the library: why? how? was it acceptable?
  * *[Gerasa](https://en.wikipedia.org/wiki/Jerash)*: [Arch](http://en.wikipedia.org/wiki/Arch_of_Hadrian_(Jerash)) [of Hadrian](http://www.atasteoftravelblog.com/the-roman-ruins-of-jerash/)
    * In general, how was the city planned? How did the buildings interact?
    * Material?
    * How did columns interact, their sizes?
    * Any motion?
    * Why the recessed pediment?
  * *[Gerasa](http://wikitravel.org/en/Jerash)*: [Nymphaeum](http://www.atlastours.net/jordan/jerash_map.html)
    * Note the placement of columns, any similarities to the Library of Celsus?
  * *[Gerasa](http://www.bibleplaces.com/gerasa.htm)* [Oval Piazza](http://www.gettyimages.com.au/creative/the-oval-piazza-stock-photos)
    * Why the shape?
    * How about streets, do they happen to have columns in Jerash? In the Eastern provinces in general? What about Italy?
  * *[Palmyra](http://en.wikipedia.org/wiki/Palmyra)*: [Tetrapylon](http://en.wikipedia.org/wiki/Tetrapylon) ([The Colonnade](http://romeartlover.tripod.com/Palmyra3.html))
    * Again, colonnaded streets. Why did they appear? Were they specific to this area?
    * Projecting brackets on the columns, what were they for? To hold statues?
    * Arches as parts of the streets
    * Made out of stone
    * Straight lintels
    * Some of those had roofs, some not
  	* [Destroyed today by ISIS](https://politota.d3.ru/palmira-tsaria-solomona-740540/)
  * *[Palmyra](http://www.sacred-destinations.com/syria/palmyra)*: [Temple](http://en.wikipedia.org/wiki/Temple_of_Bel) [of Bel](http://www.youtube.com/watch?v=NCrq_ETPG2M)
    * Determine, which parts of the building were influenced by Romans and which parts were influenced by local (or may be Greek) legacy:
        * Consecrated to the Semitic god Bel, worshipped at Palmyra in triad with the lunar god Aglibol and the sun god Yarhibol (not to Jupiter or the Capitoline triad)
        * Podium goes all the way around, serves as staircase
        * Additional staircase on the long side, not on the short one, provides the facade orientation
        * Freestanding columns go all the way around, too
        * Single cella
        * [Crenellation](https://en.wiktionary.org/wiki/crenellation) on the top
        * Some kind of a deck at the very top
  * *[Herculaneum](http://en.wikipedia.org/wiki/Herculaneum)*: [Villa](http://en.wikipedia.org/wiki/Villa_of_the_Papyri) [of](http://blogs.getty.edu/iris/a-virtual-model-of-the-villa-dei-papiri/) [the Papyri](https://sites.google.com/site/ad79eruption/herculaneum-1/villa-of-the-papyri)
    * Replicated as [The](http://www.tedsimages.com/text/gettyvil.htm) [Getty](https://vimeo.com/9404906) [Villa](https://youtu.be/Lt7Orvno_RY)
  * *West Sussex, [Chichester](http://en.wikipedia.org/wiki/Fishbourne_Roman_Palace)*: [Fishbourne](http://en.wikipedia.org/wiki/Fishbourne,_West_Sussex)
  * *[Bath](https://en.wikipedia.org/wiki/Bath,_Somerset), 19 km south-east of Bristol*: [Roman](http://en.wikipedia.org/wiki/Roman_Baths_(Bath)) [Baths](http://www.sacred-destinations.com/england/bath-roman-baths)
    * How does it compare to the Roman baths? Plan? Structure? E.g., number of tiers? Materials?
    * Brick arch, what does it tell us about the technology?
  * *[Masada](https://en.wikipedia.org/wiki/Masada)*:  [Herod’s](http://whc.unesco.org/en/list/1040) [Palace](http://www.timesofisrael.com/masada-tragic-fortress-in-the-sky/)
    * Multi-tiered structure, but why? What did they have in mind? What was the purpose of such a cascading structure?
    * What are the materials and they these particular materials were used?
    * What kind of parallels in the architectural elements may one draw to the Roman architecture?
    * Note the [hypocaust](http://en.wikipedia.org/wiki/Hypocaust), what was it used for?
  * *[Vaison](http://en.wikipedia.org/wiki/Vaison-la-Romaine)-[la](http://en.wikivoyage.org/wiki/Vaison-la-Romaine)-[Romaine](http://www.provenceweb.fr/e/vaucluse/vaison/vaison.htm)*
    * Analise the similarities between the hellenized Domus Italica and the residential architecture in Roman provinces
    * Note the introduction of two tiers buildings
    * And the evolution of the [peristyle](http://en.wikipedia.org/wiki/Peristyle)


- - - 

## Footnotes

[^1]: **Julio-Claudian dynasty**:

    Augustus                                                            27 BC – 14 AD
    
    Tiberius                                                            14-37
    
    Caligula                                                            37-41
    
    Claudius                                                            41-54
    
    Nero                                                                54-68

[^2]: **Flavian dynasty**:

    Vespasian                                                           69-79
    
    Titus                                                               79-81
    
    Domitian                                                            81-96

[^3]: **Nerva–Antonine dynasty**:

    Nerva                                                               96-98
    
    Trajan                                                              98-117
    
    Hadrian                                                             117-138
    
    Antoninus Pius                                                      138-161
    
    Lucius Verus                                                        161-169
    
    Marcus Aurelius                                                     161-180
    
    Commodus                                                            177-192

[^4]: **Year of the Five Emperors**:

    Pertinax                                                            193
    
    Didius Julianus                                                     193
    
    Pescennius Niger                                                    193
    
    Clodius Albinus                                                     193, 196-197
    
    Septimius Severus                                                   193–211

[^5]: **Severan dynasty**:

    Septimius Severus                                                   193–198
    
    *—with Caracalla*                                                   198–209
    
    *—with Caracalla and Geta*                                          209–211
    
    Caracalla and Geta                                                  211–211
    
    Caracalla                                                           211–217
    
    Interlude: Macrinus                                                 217–218
    
    Elagabalus                                                          218–222
    
    Alexander Severus                                                   222–235

[^6]: **Crisis of the Third Century**

    Carracks Emperors                                                   235–284
    
    Gordian dynasty                                                     238–244
    
    Valerian dynasty                                                    253–261
    
    Gallic Emperors                                                     260–274
    
    Illyrian Emperors                                                   268–284
    
    Caran dynasty                                                       282–285
    
    Britannic Emperors                                                  286–297

[^7]: **The Tetrarchy**

    Diocletian                                                          284–286
    
    *—with Maximian as Augustus of the West* 	                        286–293
    
    *—with Galerius and Constantius Chlorus as Caesares*                293–305
    
    Maximian as Caesar 	                                                285–286
    
    *—with Diocletian as Augustus of the East* 	                        286–305
    
    *—with Galerius and Constantius Chlorus as Caesares*                293–305
    
    Galerius and Constantius Chlorus as Caesares 	                    293–305
    
    Galerius and Constantius Chlorus as Augusti of East and West 	    305–306
    
    *—with Severus and Maximinus Daia as Caesares* 	                    305–306
    
    Galerius and Severus as Augusti of East and West 	                306–307
    
    *—with Constantine the Great and Maximinus Daia as Caesares* 	    306–307
    
    Galerius and Maxentius as Augusti of East and West 	                307–308
    
    *—with Constantine the Great and Maximinus Daia as Caesares* 	    307–308
    
    Galerius and Licinius as Augusti of East and West 	                308–311
    
    *—with Constantine the Great and Maximinus Daia as Caesares* 	    308–311
    
    Maxentius as usurper in Rome (and Asia Minor 311–312) 	            308–312
    
    Maximinus Daia and Licinius as Augusti of East and West 	        311–312
    
    *—with Constantine the Great as Caesar (self proclaimed Augustus)* 	311–312
    
    Licinius and Constantine the Great as Augusti of East and West 	    312–324
    
    *—with Constantine II, Crispus and Licinianus as Caesares* 	        317–324

[^8]: **Constantinian dynasty**

    Constantine I as Caesar 	                                        307–311

    *—with Maximinus II as Caesar*                                      307–311

    *—with Galerius and Severus as Augusti*                             307–308

    *—with Galerius and Licinius as Augusti*                            308–311

    *—with Maxentius as usurper in Rome*                                308–312

    *...and Asia Minor*                                                 311–312
    
    Constantine I as Caesar (self proclaimed Augustus) 	                311–312

    *—with Maximinus II and Licinius as Augusti of East and West*       311–312

    *The Battle of the Milvian Bridge*                                    312

    Constantine I as Augustus of the West                               312–324

    Licinius as Augustus of the East                                    312–324

    *—with Constantine II, Crispus and Licinianus as Caesares*          317–324

    Constantine I as sole Emperor                                       324–337

    Constantine II as Augustus of Gaul, Britannia and Hispania          337–340

    Constans as Augustus of Italy and Africa                            337–350

    *...and Gaul, Britannia and Hispania                                340–350

    Constantius II as Augustus of Asia and Egypt                        337–350

    Constantius II as sole Emperor                                      350-361

    Julian as Caesar                                                    355-360

    Julian as Augustus                                                  360-361

    Julian the Apostate as Sole Augustus                                361-363

[^9]: **Jovian and Valentinian dynasty**

    Jovian                                                              363-364

    Valentinian I as Emperor of the West 	                            364–375

    *—with Valens as Emperor of the East*                               364–375

    *—with Gratian as junior Augustus of West*                          375–378

    Gratian as Emperor of the West                                      375–383

    *—with Valentinian II as junior Augustus of West*                   375–378

    *—with Valens as Emperor of the East*                               375–378

    Gratian as sole emperor                                             378–379

    *—with Valentinian II as junior Augustus*                           375–379
    
    Gratian as Emperor of the West                                      379–383

    *—with Theodosius I as emperor of the East*                         379–383

    *—with Valentinian II as junior Augustus*                           375–383

    Interlude: Magnus Maximus Usurper                                   383–388

    Valentinian II in competition with Magnus Maximus in the west       383–388

    *—with Theodosius I as emperor of the East*                         375–388

    Valentinian II as Emperor of the West                               388–392

    *—with Theodosius I as emperor of the East*                         388–392
