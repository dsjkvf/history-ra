Roman Architecture by Diana E. E. Kleiner
=========================================

This is a set of lecture notes taken during the [Coursera course](https://www.coursera.org/course/romanarchitecture) *Roman Architecture by Prof. Diana E. E. Kleiner*:

  * [RA.md](https://bitbucket.org/dsjkvf/history-ra/src/master/RA.md) is a raw Markdown file
  * [RA.Rmd](https://bitbucket.org/dsjkvf/history-ra/src/master/RA.Rmd) is an [R Markdown](http://rmarkdown.rstudio.com/) project file for the compilation with [RStudio](http://www.rstudio.com/)
  * RA.css is an additional Cascading Style Sheets file needed for the HTML compilation 
  * [RA.html](https://bitbucket.org/dsjkvf/history-ra/src/master/RA.html) is a compiled HTML file ([save it](https://bitbucket.org/dsjkvf/history-ra/raw/master/RA.html) as .html to view)
  * [RA.pdf](https://bitbucket.org/dsjkvf/history-ra/src/master/RA.pdf) is a compiled PDF file (with page breaks).

In most cases, you are expected to be interested in either the Markdown file, the compiled HTML file, or the PDF document.
