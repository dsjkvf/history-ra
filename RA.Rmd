---
title: "Roman Architecture <br> by Diana E. E. Kleiner"
author: "Notes taken during the course"
date: '`r Sys.Date()`'
output:
  html_document:
    css: RA.css
    theme: spacelab
    toc: yes
---
```{r echo=FALSE}
temp <- tail(readLines("RA.md"), -3)
temp <- sub("^- - -", "<br><br><br><br>", temp)
writeLines(temp, "temp.md")
```
```{r autodoc, child='temp.md', eval=TRUE}
``` 
```{r echo=FALSE, results='hide'}
file.remove("temp.md")
```
